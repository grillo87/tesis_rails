# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180604205130) do

  create_table "activities", force: :cascade do |t|
    t.integer "area"
    t.string "title"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "proposed_by"
    t.integer "status"
    t.integer "user_type"
    t.integer "total"
    t.integer "wrong"
    t.integer "correct"
  end

  create_table "activity_students", force: :cascade do |t|
    t.integer "id_activity"
    t.integer "status"
    t.integer "id_student"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "completed"
  end

  create_table "admins", force: :cascade do |t|
    t.string "email"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
  end

  create_table "answer_students", force: :cascade do |t|
    t.integer "id_answer"
    t.integer "correct"
    t.integer "id_student"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "answers", force: :cascade do |t|
    t.string "text"
    t.integer "correct"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "activity_id"
    t.integer "total"
    t.integer "correct_total"
    t.integer "wrong_total"
    t.index ["activity_id"], name: "index_answers_on_activity_id"
  end

  create_table "approaches", force: :cascade do |t|
    t.string "content"
    t.integer "thesis_id"
    t.integer "status"
    t.string "feedback"
    t.integer "feeling"
    t.integer "teacher_feeling"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "content_english"
  end

  create_table "general_purposes", force: :cascade do |t|
    t.string "content"
    t.integer "thesis_id"
    t.integer "status"
    t.string "feedback"
    t.integer "feeling"
    t.integer "teacher_feeling"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "content_english"
  end

  create_table "messages", force: :cascade do |t|
    t.integer "id_teacher"
    t.integer "id_student"
    t.integer "id_thesis"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rule_kewords", force: :cascade do |t|
    t.integer "id_rule"
    t.string "word"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rules", force: :cascade do |t|
    t.integer "lenght"
    t.integer "area"
    t.integer "user_type"
    t.integer "created_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "session_theses", force: :cascade do |t|
    t.string "content"
    t.integer "area"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
  end

  create_table "specific_purposes", force: :cascade do |t|
    t.string "content"
    t.integer "thesis_id"
    t.integer "status"
    t.string "feedback"
    t.integer "feeling"
    t.integer "teacher_feeling"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "content_english"
    t.integer "version"
  end

  create_table "students", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "student_identification"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password"
    t.string "password_digest"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "teachers", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.integer "nationality"
    t.string "identification_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password"
    t.string "password_digest"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
  end

  create_table "theses", force: :cascade do |t|
    t.integer "student_id"
    t.integer "teacher_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "thesis_subjects", force: :cascade do |t|
    t.integer "thesis_id"
    t.integer "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "titles", force: :cascade do |t|
    t.string "content"
    t.integer "thesis_id"
    t.integer "status"
    t.string "feedback"
    t.integer "feeling"
    t.integer "teacher_feeling"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "content_english"
  end

end
