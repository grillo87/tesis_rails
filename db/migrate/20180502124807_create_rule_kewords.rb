class CreateRuleKewords < ActiveRecord::Migration[5.1]
  def change
    create_table :rule_kewords do |t|
      t.integer :id_rule
      t.string :word

      t.timestamps
    end
  end
end
