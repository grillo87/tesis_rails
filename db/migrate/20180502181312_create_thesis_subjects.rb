class CreateThesisSubjects < ActiveRecord::Migration[5.1]
  def change
    create_table :thesis_subjects do |t|
      t.integer :thesis_id
      t.integer :subject_id

      t.timestamps
    end
  end
end
