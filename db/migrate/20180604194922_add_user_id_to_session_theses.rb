class AddUserIdToSessionTheses < ActiveRecord::Migration[5.1]
  def change
    add_column :session_theses, :user_id, :integer
  end
end
