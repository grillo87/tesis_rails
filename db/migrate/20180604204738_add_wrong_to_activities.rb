class AddWrongToActivities < ActiveRecord::Migration[5.1]
  def change
    add_column :activities, :wrong, :integer
  end
end
