class CreateApproaches < ActiveRecord::Migration[5.1]
  def change
    create_table :approaches do |t|
      t.string :content
      t.integer :thesis_id
      t.integer :status
      t.string :feedback
      t.integer :feeling
      t.integer :teacher_feeling

      t.timestamps
    end
  end
end
