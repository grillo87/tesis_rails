class CreateSessionTheses < ActiveRecord::Migration[5.1]
  def change
    create_table :session_theses do |t|
      t.string :content
      t.integer :area

      t.timestamps
    end
  end
end
