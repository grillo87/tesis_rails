class AddWrongToAnswers < ActiveRecord::Migration[5.1]
  def change
    add_column :answers, :wrong_total, :integer
  end
end
