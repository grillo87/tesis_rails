class CreateAnswerStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :answer_students do |t|
      t.integer :id_answer
      t.integer :correct
      t.integer :id_student

      t.timestamps
    end
  end
end
