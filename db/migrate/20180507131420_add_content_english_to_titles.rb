class AddContentEnglishToTitles < ActiveRecord::Migration[5.1]
  def change
    add_column :titles, :content_english, :string
  end
end
