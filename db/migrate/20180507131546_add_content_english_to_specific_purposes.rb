class AddContentEnglishToSpecificPurposes < ActiveRecord::Migration[5.1]
  def change
    add_column :specific_purposes, :content_english, :string
  end
end
