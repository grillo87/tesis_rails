class AddActivityToAnswers < ActiveRecord::Migration[5.1]
  def change
    add_reference :answers, :activity, index: true
  end
end
