class AddCorrectToActivities < ActiveRecord::Migration[5.1]
  def change
    add_column :activities, :correct, :integer
  end
end
