class AddTotalToAnswers < ActiveRecord::Migration[5.1]
  def change
    add_column :answers, :total, :integer
  end
end
