class AddCompletedToActivityStudent < ActiveRecord::Migration[5.1]
  def change
    add_column :activity_students, :completed, :integer
  end
end
