class CreateActivityStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :activity_students do |t|
      t.integer :id_activity
      t.integer :status
      t.integer :id_student

      t.timestamps
    end
  end
end
