class AddVersionToSpecificPurposes < ActiveRecord::Migration[5.1]
  def change
    add_column :specific_purposes, :version, :integer
  end
end
