class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.integer :id_teacher
      t.integer :id_student
      t.integer :id_thesis

      t.timestamps
    end
  end
end
