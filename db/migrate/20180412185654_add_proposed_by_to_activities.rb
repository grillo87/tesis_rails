class AddProposedByToActivities < ActiveRecord::Migration[5.1]
  def change
    add_column :activities, :proposed_by, :integer
  end
end
