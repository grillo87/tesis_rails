class AddContentEnglishToGeneralPurposes < ActiveRecord::Migration[5.1]
  def change
    add_column :general_purposes, :content_english, :string
  end
end
