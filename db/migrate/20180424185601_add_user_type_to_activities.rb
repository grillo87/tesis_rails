class AddUserTypeToActivities < ActiveRecord::Migration[5.1]
  def change
    add_column :activities, :user_type, :integer
  end
end
