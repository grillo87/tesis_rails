class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset_admin(admin)
    @admin = admin
    mail to: admin.email, subject: "Recuperación de Contraseña para SAEP"
  end

  def password_reset_student(student)
    @student = student
    mail to: student.email, subject: "Recuperación de Contraseña para SAEP"
  end

  def password_reset_teacher(teacher)
    @teacher = teacher
    mail to: teacher.email, subject: "Recuperación de Contraseña para SAEP"
  end

end
