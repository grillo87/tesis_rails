class SessionsController < ApplicationController

  def new

  end


  def recover_password


  end


  def edit_password

  end

  def update_password

  end

  def create
    student = Student.find_by(email: params[:session][:email].downcase)

    if student && student.authenticate(params[:session][:password])

      log_in_student student
      user_type 1
      redirect_to :action => 'index_student', :controller => "thesis"

    else

      teacher = Teacher.find_by(email: params[:session][:email].downcase)
      if teacher && teacher.authenticate(params[:session][:password])

        log_in_teacher teacher
        user_type 2
        redirect_to :action => 'list_students_requets', :controller => "teachers"

      else

        admin = Admin.find_by(email: params[:session][:email].downcase)

        if admin && admin.authenticate(params[:session][:password])

          log_in_admin admin
          user_type 3
          redirect_to :action => 'index', :controller => "teachers"

        else

          flash[:danger] = 'Correo Electrónico/Contraseña inválidos'
          redirect_to :action => 'new', :controller => "sessions"

        end


      end


    end
  end

  def unathorized

    log_out_user
    redirect_to root_url

  end

  def destroy

    log_out_user
    flash[:warning] = 'Su sesión ha sido cerrada exitosamente'
    redirect_to root_url

  end


end
