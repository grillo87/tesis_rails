class RulesController < ApplicationController
  def new
  end

  def create
  end

  def update
  end

  def edit
  end

  def destroy
  end

  def rule_teacher

    @rule = Rule.find(params[:id])
    @keywords = RuleKeword.where(id_rule: params[:id]).paginate(:page => params[:page], :per_page => 5)

  end

  def update_rule_admin

    @rules = Rule.where(area: params[:rule][:area_value], user_type: 3)

    if (@rules.first.nil?)

      @rule = Rule.new

      if (params[:rule][:lenght] != "")

        @rule.lenght = params[:rule][:lenght]

      else

        @rule.lenght = 0

      end

      if params[:rule][:area_value] == '0'
        @area = "Titulo"
      elsif params[:rule][:area_value] == '1'
        @area = "Planteamiento"
      elsif params[:rule][:area_value] == '2'
        @area = "Obj_General"
      else
        @area = "Obj_Especifico"
      end

      @rule.area = @area

      @rule.user_type = 3
      @rule.created_by = params[:rule][:created_by]

      if @rule.save

        RuleKeword.where(:id_rule => @rule.id).destroy_all

        if (params[:rule][:keywords] != "")

          @keywords = params[:rule][:keywords].split("*")

          @keywords.each do |keyword|

            @keywordRegister = RuleKeword.new
            @keywordRegister.word = keyword
            @keywordRegister.id_rule = @rule.id
            @keywordRegister.save

          end


        end

        flash[:success] = "La regla ha sido generada con éxito"
        @rules = Rule.paginate(:page => params[:page], :per_page => 5)
        @teachers = Teacher.all
        @admins = Admin.all
        render 'index'


      else

        flash[:danger] = "No ha sido posible generar la regla, por favor revisar"
        @rules = Rule.where(area: params[:area], user_type: 3)
        @areasValue = params[:area]

        if (@rules.first.nil?)

          @rule = Rule.new
          if params[:area] == '0'
            @area = "Título"
          elsif params[:area] == '1'
            @area = "Planteamiento del Problema"
          elsif params[:area] == '2'
            @area = "Objetivo General"
          else
            @area = "Objetivos Específicos"
          end

          @keywords = Array.new

        else

          @rule = @rules.first
          if @rule.area == "Titulo"
            @area = "Título"
          elsif @rule.area == "Planteamiento"
            @area = "Planteamiento del Problema"
          elsif @rule.area == "Obj_General"
            @area = "Objetivo General"
          else
            @area = "Objetivos Específicos"
          end

          @keywords = RuleKeword.where(id_rule: @rule.id)

        end

        render 'rule_admin'


      end


    else

      @rule = @rules.first

      if (params[:rule][:lenght] != "")

        @rule.lenght = params[:rule][:lenght]

      else

        @rule.lenght = 0

      end

      @rule.created_by = params[:rule][:created_by]

      if @rule.save

        RuleKeword.where(:id_rule => @rule.id).destroy_all

        if (params[:rule][:keywords] != "")

          @keywords = params[:rule][:keywords].split("*")

          @keywords.each do |keyword|

            @keywordRegister = RuleKeword.new
            @keywordRegister.word = keyword
            @keywordRegister.id_rule = @rule.id
            @keywordRegister.save

          end


        end

        flash[:success] = "La regla ha sido generada con éxito"
        @rules = Rule.paginate(:page => params[:page], :per_page => 5)
        @teachers = Teacher.all
        @admins = Admin.all
        render 'index'

      else

        flash[:danger] = "No ha sido posible generar la regla, por favor revisar"
        @rules = Rule.where(area: params[:area], user_type: 3)
        @areasValue = params[:area]

        if (@rules.first.nil?)

          @rule = Rule.new
          if params[:area] == '0'
            @area = "Título"
          elsif params[:area] == '1'
            @area = "Planteamiento del Problema"
          elsif params[:area] == '2'
            @area = "Objetivo General"
          else
            @area = "Objetivos Específicos"
          end

          @keywords = Array.new

        else

          @rule = @rules.first
          if @rule.area == "Titulo"
            @area = "Título"
          elsif @rule.area == "Planteamiento"
            @area = "Planteamiento del Problema"
          elsif @rule.area == "Obj_General"
            @area = "Objetivo General"
          else
            @area = "Objetivos Específicos"
          end

          @keywords = RuleKeword.where(id_rule: @rule.id)

        end

        render 'rule_admin'


      end


    end

  end

  def rule_admin

    @rules = Rule.where(area: params[:area], user_type: 3)

    @areasValue = params[:area]

    if (@rules.first.nil?)

      @rule = Rule.new
      if params[:area] == '0'
        @area = "Título"
      elsif params[:area] == '1'
        @area = "Planteamiento del Problema"
      elsif params[:area] == '2'
        @area = "Objetivo General"
      else
        @area = "Objetivos Específicos"
      end

      @keywords = Array.new

    else

      @rule = @rules.first
      if @rule.area == "Titulo"
        @area = "Título"
      elsif @rule.area == "Planteamiento"
        @area = "Planteamiento del Problema"
      elsif @rule.area == "Obj_General"
        @area = "Objetivo General"
      else
        @area = "Objetivos Específicos"
      end

      @keywords = RuleKeword.where(id_rule: @rule.id)

    end

  end

  def index

    @rules = Rule.paginate(:page => params[:page], :per_page => 5)
    @teachers = Teacher.all
    @admins = Admin.all

  end

  def index_teacher_rules

  end


  def edit_rules_teacher

    @rules = Rule.where(user_type: "2", created_by: session[:user_id], area: params[:area])
    @areasValue = params[:area]
    if params[:area] == '0'
      @area = "Título"
    elsif params[:area] == '1'
      @area = "Planteamiento del Problema"
    elsif params[:area] == '2'
      @area = "Objetivo General"
    else
      @area = "Objetivos Específicos"
    end

    if (@rules.count == 0)

      @rule = Rule.new
      @keywords = Array.new
      @valueK = ""

    else

      @rule = @rules.first
      @keywords = RuleKeword.where(id_rule: @rule.id)

      @valueK = ""
      @keywords.each do |keyword|

        if @valueK == ""
          @valueK = keyword.word
        else
          @valueK = @valueK + "*" + keyword.word
        end

      end


    end

  end

  def update_rule_teacher

    @rules = Rule.where(user_type: "2", created_by: session[:user_id], area: params[:rule][:area_value])

    if (@rules.first.nil?)

      @rule = Rule.new

      if (params[:rule][:lenght] != "")

        @rule.lenght = params[:rule][:lenght]

      else

        @rule.lenght = 0

      end

      if params[:rule][:area_value] == '0'
        @area = "Titulo"
      elsif params[:rule][:area_value] == '1'
        @area = "Planteamiento"
      elsif params[:rule][:area_value] == '2'
        @area = "Obj_General"
      else
        @area = "Obj_Especifico"
      end

      @rule.area = @area

      @rule.user_type = 2
      @rule.created_by = params[:rule][:created_by]

      if @rule.save

        if (params[:rule][:keywords] != "")

          @keywords = params[:rule][:keywords].split("*")

          @keywords.each do |keyword|

            @keywordRegister = RuleKeword.new
            @keywordRegister.word = keyword
            @keywordRegister.id_rule = @rule.id
            @keywordRegister.save

          end


        end

        flash[:success] = "La regla ha sido generada con éxito"
        redirect_to(:action => 'index_teacher_rules', :controller => "rules") and return


      else

        flash[:danger] = "No ha sido posible generar la regla, por favor revisar"

        @areasValue = params[:rule][:area_value]
        if @areasValue == '0'
          @area = "Título"
        elsif @areasValue == '1'
          @area = "Planteamiento del Problema"
        elsif @areasValue == '2'
          @area = "Objetivo General"
        else
          @area = "Objetivos Específicos"
        end

        @rule = Rule.new
        @valueK = params[:rule][:keywords]
        @size = @valueK.split("*").count
        @keywords = Array.new(@size)

        render 'edit_rules_teacher'


      end


    else

      @rule = @rules.first

      if (params[:rule][:lenght] != "")

        @rule.lenght = params[:rule][:lenght]

      else

        @rule.lenght = 0

      end

      @rule.created_by = params[:rule][:created_by]
      @rule.user_type = 2

      if @rule.save

        RuleKeword.where(:id_rule => @rule.id).destroy_all

        if (params[:rule][:keywords] != "")

          @keywords = params[:rule][:keywords].split("*")

          @keywords.each do |keyword|

            @keywordRegister = RuleKeword.new
            @keywordRegister.word = keyword
            @keywordRegister.id_rule = @rule.id
            @keywordRegister.save

          end


        end

        flash[:success] = "La regla ha sido generada con éxito"
        redirect_to(:action => 'index_teacher_rules', :controller => "rules") and return

      else

        flash[:danger] = "No ha sido posible generar la regla, por favor revisar"

        @areasValue = params[:rule][:area_value]
        if @areasValue == '0'
          @area = "Título"
        elsif @areasValue == '1'
          @area = "Planteamiento del Problema"
        elsif @areasValue == '2'
          @area = "Objetivo General"
        else
          @area = "Objetivos Específicos"
        end

        @valueK = params[:rule][:keywords]
        @size = @valueK.split("*").count
        @keywords = Array.new(@size)

        render 'edit_rules_teacher'


      end


    end

  end

  def show
  end
end
