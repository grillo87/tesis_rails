class TeachersController < ApplicationController
  def new

    @teacher = Teacher.new

  end

  def create

    @teacher = Teacher.new(teacher_params)
    if @teacher.save

      flash[:success] = "El profesor ha sido registrado"
      @teachers = Teacher.paginate(:page => params[:page], :per_page => 5)
      render 'index'

    else
      render 'new'
    end

  end

  def update

    @teacher = Teacher.find(params[:id])
    if @teacher.update_attributes(teacher_edit_params)
      flash[:success] = "El profesor ha sido actualizado"
      @teachers = Teacher.paginate(:page => params[:page], :per_page => 5)
      render 'show'
    else
      render 'edit'
    end

  end


  def edit_student_request_thesis

    Message.where(id_thesis: params[:thesis_id]).destroy_all

    @message = Message.new
    @message.id_teacher = params[:teacher_id]
    @message.id_student = params[:student_id]
    @message.id_thesis = params[:thesis_id]

    if (@message.save)

      flash[:success] = "Su solicitud para el profesor ha sido enviada con éxito!"
      redirect_to(:action => 'index_student', :controller => "thesis") and return


    else

      flash[:danger] = "Su solicitud no ha podido ser enviada, por favor intente nuevamente!"
      redirect_to(:action => 'edit_thesis_teacher_select', :controller => "thesis", :thesis_id => params[:thesis_id]) and return

    end

  end

  def list_students_requets

    @messages = Message.where(id_teacher: session[:user_id]).paginate(:page => params[:page], :per_page => 5)
    @students = Array.new

    @messages.each do |message|

      @student = Student.find(message.id_student)
      @students.push(@student)

    end

  end


  def student_request_thesis

    @message = Message.new
    @message.id_teacher = params[:teacher_id]
    @message.id_student = params[:student_id]
    @message.id_thesis = params[:thesis_id]


    if (@message.save)

      flash[:success] = "Su solicitud para el profesor ha sido enviada con éxito!"
      redirect_to(:action => 'index_student', :controller => "thesis") and return


    else

      flash[:danger] = "Su solicitud no ha podido ser enviada, por favor intente nuevamente!"
      redirect_to(:action => 'thesis_teacher_select', :controller => "thesis", :thesis_id => params[:thesis_id]) and return

    end

  end


  def update_teacher_activity_admin

    if Activity.update(params[:id], status: params[:status])

      if params[:status] == "Aprobada"
        flash[:success] = "La actividad ha sido aprobada éxitosamente"
      else
        flash[:success] = "La actividad ha sido rechazada éxitosamente"
      end

      if params[:origin] == "0"

        @activity = Activity.find(params[:id])
        @activities = Activity.where(proposed_by: @activity.proposed_by, user_type: 2).paginate(:page => params[:page], :per_page => 5)

        render 'activities_admin'

      else

        @activities = Activity.paginate(:page => params[:page], :per_page => 5)
        @teachers = Teacher.all
        @admins = Admin.all

        redirect_to :action => 'index', :controller => "activities"

      end


    else

      flash[:danger] = 'No ha sido posible actualizar el estado de la actividad propuesta'

      if params[:origin] == "0"

        @activity = Activity.find(params[:id])
        @answers = Answer.where(activity_id: params[:id]).paginate(:page => params[:page], :per_page => 5)

        render 'activity_teacher_admin'

      else

        @activities = Activity.paginate(:page => params[:page], :per_page => 5)
        @teachers = Teacher.all
        @admins = Admin.all

        redirect_to :action => 'index', :controller => "activities"

      end

    end

  end


  def activity_teacher_admin

    @origin = params[:origin]
    @activity = Activity.find(params[:id])
    @answers = Answer.where(activity_id: params[:id]).paginate(:page => params[:page], :per_page => 5)

  end

  def activities_admin

    @activities = Activity.where(proposed_by: params[:id], user_type: 2).order(:area).paginate(:page => params[:page], :per_page => 5)

  end

  def edit

    @teacher = Teacher.find(params[:id])

  end

  def destroy

    Teacher.find(params[:id]).destroy
    flash[:success] = "El profesor ha sido eliminado"
    redirect_to teachers_url

  end

  def index

    @teachers = Teacher.paginate(:page => params[:page], :per_page => 5)

  end

  def show

    @teachers = Teacher.paginate(:page => params[:page], :per_page => 5)

  end

  def teacher_edit_params
    params.require(:teacher).permit(:first_name, :last_name, :email, :nationality, :identification_number)
  end

  def teacher_params
    params.require(:teacher).permit(:first_name, :last_name, :email, :password, :nationality, :identification_number)
  end

end
