class PasswordResetsController < ApplicationController

  before_action :get_user, only: [:edit, :update]

  def get_user

    if (params[:user_type] == 1)

      @user = Student.find_by(email: params[:email])
      @user_type = 1

    elsif (params[:user_type] == 2)

      @user = Teacher.find_by(email: params[:email])
      @user_type = 2

    else

      @user = Admin.find_by(email: params[:email])
      @user_type = 3

    end

  end


  def new
  end

  #Esta funcionalidad NO fue implementada en la version final, aquí la idea era generar un correo para recuperar la contraseña
  # del usuario

  def create

    #NO IMPLEMENTADO EN VERSION USADA EN PRUEBAS

    @student = Student.find_by(email: params[:password_reset][:email].downcase)

    if @student

      @student.create_reset_digest
      @student.send_password_reset_email
      flash[:info] = "Ha sido enviado un Email con las instrucciones para recuperar su contraseña"
      redirect_to root_url

    else

      #NO IMPLEMENTADO EN VERSION USADA EN PRUEBAS

      @teacher = Teacher.find_by(email: params[:password_reset][:email].downcase)

      if @teacher

        @teacher.create_reset_digest
        @teacher.send_password_reset_email
        flash[:info] = "Ha sido enviado un Email con las instrucciones para recuperar su contraseña"
        redirect_to root_url

      else

        #NO IMPLEMENTADO EN VERSION USADA EN PRUEBAS

        @admin = Admin.find_by(email: params[:password_reset][:email].downcase)

        if @admin

          @admin.create_reset_digest
          @admin.send_password_reset_email
          flash[:info] = "Ha sido enviado un Email con las instrucciones para recuperar su contraseña"
          redirect_to root_url

        else

          flash[:danger] = 'Correo Electrónico no registrado'
          redirect_to :action => 'recover_password', :controller => "sessions"

        end


      end


    end

  end

  def edit

  end

  #Esta seccion no fue utilizada en la version final, su finalidad era la de recuperar contraseña en caso de que el usuario
  # no pudiese ingresar al sistema

  def update

    if params[:user_type] == 3

      if @user.update_attributes(user_params)

        flash[:success] = "Su contraseña ha sido actualizada"
        redirect_to root_path

      else

        flash[:danger] = 'No ha sido posible actualizar su contraseña'
        render 'edit'

      end


    end

  end


  #Esta seccion no fue utilizada en la version final, su finalidad era la de recuperar contraseña en caso de que el usuario
  # no pudiese ingresar al sistema

  def change_password

    if params[:user][:password].empty?

      flash[:danger] = 'El campo contraseña no puede ser vacío'
      redirect_to root_path

    elsif params[:user][:password] != params[:user][:password_confirmation]

      flash[:danger] = 'La contraseñas no coinciden, por favor verificar'
      redirect_to root_path

    else

      if params[:user][:user_type] == '1'

        student = Student.find_by(email: params[:user][:email])

        if student.update(password: params[:user][:password]) # Case (4)

          flash[:success] = "Su contraseña ha sido actualizada"
          redirect_to root_path

        else

          flash[:danger] = 'No ha sido posible actualizar su contraseña'
          redirect_to root_path

        end

      end

      if params[:user][:user_type] == '2'

        teacher = Teacher.find_by(email: params[:user][:email])

        if teacher.update(password: params[:user][:password]) # Case (4)

          flash[:success] = "Su contraseña ha sido actualizada"
          redirect_to root_path

        else

          flash[:danger] = 'No ha sido posible actualizar su contraseña'
          redirect_to root_path

        end

      end

      if params[:user][:user_type] == '3'

        admin = Admin.find_by(email: params[:user][:email])

        if admin.update(password: params[:user][:password]) # Case (4)

          flash[:success] = "Su contraseña ha sido actualizada"
          redirect_to root_path

        else

          flash[:danger] = 'No ha sido posible actualizar su contraseña'
          redirect_to root_path

        end

      end


    end

  end


end
