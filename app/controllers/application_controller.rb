class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def log_in_teacher(teacher)
    session[:user_id] = teacher.id
    session[:username] = teacher.first_name + " " + teacher.last_name
  end

  def log_in_student(student)
    session[:user_id] = student.id
    session[:username] = student.first_name + " " + student.last_name
  end

  def log_in_admin(admin)
    session[:user_id] = admin.id
    session[:username] = admin.email
  end

  def store_title_session(title)
    session[:title] = title
  end

  def store_approach_session(approach)
    session[:approach] = approach
  end

  def store_general_purpose_session(general_purpose)
    session[:general_purpose] = general_purpose
  end

  def store_specific_purposes_session(specific_purposes)
    session[:specific_purposes] = specific_purposes
  end


  def user_type(type)
    session[:user_type] = type
  end

  def log_out_user
    session.delete(:user_id)
    session.delete(:user_type)
  end

end
