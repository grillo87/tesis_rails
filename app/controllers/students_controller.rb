class StudentsController < ApplicationController
  def new

    @student = Student.new

  end


  def show_thesis_student

    @thesis = Thesis.where(student_id: params[:id_student], teacher_id: session[:user_id]).first

    @titles = Title.where(thesis_id: @thesis.id).order(created_at: :desc).first
    @approach = Approach.where(thesis_id: @thesis.id).order(created_at: :desc).first
    @generalPurpose = GeneralPurpose.where(thesis_id: @thesis.id).order(created_at: :desc).first
    @specificPurpose = SpecificPurpose.where(thesis_id: @thesis.id).order(created_at: :desc).first

  end


  def students_teacher_list

    @thesis = Thesis.where(teacher_id: session[:user_id])
    @students = Array.new

    @thesis.each do |thesis|

      @student = Student.find(thesis.student_id)
      @students.push(@student)

    end

  end

  def create

    @student = Student.new(student_params)
    if @student.save

      flash[:success] = "El estudiante ha sido registrado"
      @students = Student.paginate(:page => params[:page], :per_page => 5)
      render 'index'

    else
      render 'new'
    end

  end

  def update

    @student = Student.find(params[:id])
    if @student.update_attributes(student_edit_params)
      flash[:success] = "El estudiante ha sido actualizado"
      @students = Student.paginate(:page => params[:page], :per_page => 5)
      render 'index'
    else
      render 'edit'
    end

  end

  def edit

    @student = Student.find(params[:id])
    @thesis = Thesis.where(student_id: params[:id]).first

  end

  def destroy

    Student.find(params[:id]).destroy
    flash[:success] = "El estudiante ha sido eliminado"
    redirect_to students_url

  end

  def index

    @students = Student.paginate(:page => params[:page], :per_page => 5)

  end

  def show
  end


  def student_edit_params
    params.require(:student).permit(:first_name, :last_name, :email, :student_identification)
  end

  def student_params
    params.require(:student).permit(:first_name, :last_name, :email, :password, :student_identification)
  end

end
