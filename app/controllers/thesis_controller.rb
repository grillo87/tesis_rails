class ThesisController < ApplicationController

  #Gema encargada de obtener las categorias de las distintas partes de la Tesis
  require 'highscore'


  #Funcion encargada de verificar si el contenido es vacio

  def emptyContent(content)

    if ((content != "") && (content != " "))

      return false

    else

      return true

    end

  end

  #Funcion encargada de verificar si la regla existe
  # tipo de usuario (2 => Profesor, 3 => Coordinacion)
  # area (Titulo, Planteamiento, Obj_General, Obj_Especifico)
  # creator (Id de usuario en caso de que sea de tipo 2 para obtener las reglas del profesor)

  def validateRuleExists(userType, area, creator)

    if (creator.to_i != -1)

      @ruleValidation = Rule.where(user_type: userType, area: area, created_by: creator.to_i).first

    else

      @ruleValidation = Rule.where(user_type: userType, area: area).first

    end


    if (@ruleValidation.present?)

      return true

    else

      return false

    end


  end

  #Funcion encargada de verificar si el contenido satisface la regla existente para el area
  # tipo de usuario (2 => Profesor, 3 => Coordinacion)
  # area (Titulo, Planteamiento, Obj_General, Obj_Especifico)
  # creator (Id de usuario en caso de que sea de tipo 2 para obtener las reglas del profesor)

  def validateThesisArea(content, userType, area, creator)

    if (creator.to_i != -1)

      @ruleValidation = Rule.where(user_type: userType, area: area, created_by: creator.to_i).first

    else

      @ruleValidation = Rule.where(user_type: userType, area: area).first

    end

    if (@ruleValidation.present?)

      @ruleKeywords = RuleKeword.where(id_rule: @ruleValidation.id)

      if ((@ruleValidation.lenght != 0) && (content.length > @ruleValidation.lenght))

        case area
          when "Titulo"
            return "El Título de la propuesta no debe superar los " + @ruleValidation.lenght.to_s + " carácteres"
          when "Planteamiento"
            return "El Planteamiento del problema de la propuesta no debe superar los " + @ruleValidation.lenght.to_s + " carácteres"
          when "Obj_General"
            return "El Objetivo General de la propuesta no debe superar los " + @ruleValidation.lenght.to_s + " carácteres"
          when "Obj_Especifico"
            return "Los Objetivos Específicos de la propuesta no deben superar los " + @ruleValidation.lenght.to_s + " carácteres"
          else
            return "Error inesperado"
        end

      elsif @ruleKeywords.count != 0

        @wordsRule = ""
        @wordRuleCounter = 0

        @ruleKeywords.each do |word|

          #Se va generando la respuesta con las palabras claves por si hace falta mostrarlo al usuario

          if @wordsRule == ""

            @wordsRule = word.word

          else

            @wordsRule = @wordsRule + ", " + word.word

          end

          if content.upcase.include? word.word.upcase

            @wordRuleCounter = @wordRuleCounter + 1

          end

        end


        if (@wordRuleCounter == 0)

          case area
            when "Titulo"
              return "El Título de la propuesta debe contener algunas de las siguientes palabras " + @wordsRule
            when "Planteamiento"
              return "El Planteamiento del problema de la propuesta debe contener algunas de las siguientes palabras " + @wordsRule
            when "Obj_General"
              return "El Objetivo general de la propuesta debe contener algunas de las siguientes palabras " + @wordsRule
            when "Obj_Especifico"
              return "Los Objetivos Específicos de la propuesta deben contener algunas de las siguientes palabras " + @wordsRule
            else
              return "Error inesperado"
          end

        else

          return "NA"

        end


      else

        return "NA"

      end

    else

      return "NA"

    end

  end

  #Funcion encargada de extraer las categorias para el contenido, en caso de existir asocia el mismo con la tesis
  # caso contrario registra la palabra clave y genera la relacion con el contenido
  # contenido (Texto del area a extraer categorias)
  # id de la tesis relacionada al contenido

  def getAndStoreTextKeywords(content, idThesis)

    @textKeywords = Highscore::Content.new content
    @textKeywords.configure do
      set :multiplier, 2
      set :upper_case, 3
      set :long_words, 2
      set :long_words_threshold, 15
      set :short_words_threshold, 3
      set :bonus_multiplier, 2
      set :ignore_case, true
      set :word_pattern, /[\w]+[^\s0-9]/
    end

    @textKeywords.keywords.top(50).each do |keyword|

      @subject = Subject.where(name: keyword.text)

      if (@subject.first.nil?)

        @subjectNot = Subject.new
        @subjectNot.name = keyword.text
        @subjectNot.save

        @thesisSubject = ThesisSubject.new
        @thesisSubject.thesis_id = idThesis
        @thesisSubject.subject_id = @subjectNot.id

        @thesisSubject.save

      else

        @thesisSubF = ThesisSubject.where(thesis_id: idThesis, subject_id: @subject.first.id)

        if @thesisSubF.first.nil?

          @thesisSubject = ThesisSubject.new
          @thesisSubject.thesis_id = idThesis
          @thesisSubject.subject_id = @subject.first.id

          @thesisSubject.save

        end


      end

    end



  end



  def new

    @thesis = Thesis.new
    @rules = Rule.all

  end

  def create
  end

  def update
  end

  def edit
  end

  def destroy


    #Funcion encargada de eliminar la Tesis a nivel de usuario y todos los elementos relacionados
    # Titulo, Planteamiento, Objetivos Generales y Específicos, Mensajes (Solicitudes de tutoría a profesores)
    # Tópicos relacionados a la tesis

    Thesis.find(params[:id]).destroy
    Title.where(thesis_id: params[:id]).destroy_all
    Approach.where(thesis_id: params[:id]).destroy_all
    GeneralPurpose.where(thesis_id: params[:id]).destroy_all
    SpecificPurpose.where(thesis_id: params[:id]).destroy_all
    Message.where(id_thesis: params[:id]).destroy_all
    ThesisSubject.where(thesis_id: params[:id]).destroy_all
    flash[:success] = "Su Propuesta de Tesis ha sido eliminada"
    redirect_to(:action => 'index_student', :controller => "thesis") and return

  end

  def index
  end

  def show
  end

  def thesis_new_student

    #Encargado de generar el formulario de introducción de las partes de la tesis para el estudiante
    # Los contadores son los verificadores de indicar si el usuario se ha equivocado menos de tres veces
    # Si dichos contadores llega a ser mayor o igual a 3 pasa a sugerir las actividades para dicha area de la propuesta

    @thesis = Thesis.new
    @titleCounter = 0
    @approachCounter = 0
    @generalPurposeCounter = 0
    @specificPurposeCounter = 0

  end

  def thesis_teacher_select

    #Función encargada de mostrar las sugerencias de tutores para la tesis en base a las tesis donde anteriormente este sea
    # tutor, obtiene las categorías asociadas y verifica si la propuesta que se esta generando actualmente tiene match con
    # alguna de las previas del profesor

    @topicsList = ThesisSubject.where(thesis_id: params[:thesis_id])
    @teachers = Teacher.all

    @thesisId = params[:thesis_id]
    @thesisList = Thesis.where(id: @thesisId)
    @studentId = @thesisList.first.student_id

    #Se genera un arreglo con la lista de profesores que tengan temas de tesis asociados

    @teachersTopics = Array.new

    @topicsList.each do |topics|

      #Se optiene la lista de categorias asociadas a la tesis

      @thesisTopic = ThesisSubject.where(subject_id: topics.subject_id)

      @thesisTopic.each do |thesis|

        #Se verifica que la categoria no sea la misma de la tesis que se esta creando

        if thesis.thesis_id != params[:thesis_id]

          @thesisFound = Thesis.find(thesis.thesis_id)

          #En caso de que la tesis tenga temas asociados procede entonces a obtener el id del profesor
          # Dicho profesor sera el que se agrega al arreglo, se verifica que el profesor no sea nulo
          # ya que se puede tratar de una tesis que todavía no tenga profesor (Tutor)

          if (@thesisFound.teacher_id.nil?)

          else

            @found = false

            @teachersTopics.each do |theacherWithThesis|

              if theacherWithThesis == @thesisFound.teacher_id

                @found = true

              end


            end


            #Si se encontro el profesor, en este caso se agrega al arreglo a ser mostrado en la vista

            if @found == false

              @teachersTopics.push(@thesisFound.teacher_id)

            end


          end

        end


      end

    end


  end

  def thesis_new_repeat_student

    #En esta funcion se cae en caso de que el estudiante este generando su propuesta de trabajo de grado
    # y no cumpla con alguna de las reglas, en cuyo caso se obtienen las partes asociadas (Titulo, Planteamiento del Problema,
    # Objetivo General y Específicos), se almacenan los mismos en una tabla temporal ya que por la cantidad de texto que los
    # mismos pueden llegar a contener no es posible enviarlos por el URL mediante un POST

    @thesis = Thesis.new

    @sessionTitle = SessionThesis.where(area: 0, user_id: session[:user_id]).first
    @sessionApproach = SessionThesis.where(area: 1, user_id: session[:user_id]).first
    @sessionGeneral = SessionThesis.where(area: 2, user_id: session[:user_id]).first
    @sessionSpecific = SessionThesis.where(area: 3, user_id: session[:user_id]).first

    @thesis.title = @sessionTitle.content
    @thesis.approach = @sessionApproach.content
    @thesis.general_purpose = @sessionGeneral.content
    @thesis.specifics = @sessionSpecific.content

    #Se limpia la tabla temporal con los elementos del estudiante de la propuesta de Trabajo de Grado

    SessionThesis.where(user_id: session[:user_id]).delete_all

    @titleCounter = params[:titleCounter]
    @approachCounter = params[:approachCounter]
    @generalPurposeCounter = params[:generalCounter]
    @specificPurposeCounter = params[:specificCounter]

    render 'thesis_new_student'


  end

  def thesis_create_student

    #Se genera la propuesta en la BD con los elementos de la propuesta, se almacenan en una tabla temporal, caso tal de
    # que ocurra incumplimiento por parte del estudiante de las reglas y se tenga que volver a mostrar el formulario

    @title = params[:thesis][:title]
    @approach = params[:thesis][:approach]
    @generalPurpose = params[:thesis][:general_purpose]
    @specificsPurposes = params[:thesis][:specifics].split("*")

    @sessionTitle = SessionThesis.new
    @sessionApproach = SessionThesis.new
    @sessionGeneral = SessionThesis.new
    @sessionSpecific = SessionThesis.new

    #Titulo de propuesta

    @sessionTitle.content = @title
    @sessionTitle.area = 0
    @sessionTitle.user_id = session[:user_id]

    #Planteamiento del problema

    @sessionApproach.content = @approach
    @sessionApproach.area = 1
    @sessionApproach.user_id = session[:user_id]

    #Objetivo General

    @sessionGeneral.content = @generalPurpose
    @sessionGeneral.area = 2
    @sessionGeneral.user_id = session[:user_id]

    #Objetivos específicos

    @sessionSpecific.content = params[:thesis][:specifics]
    @sessionSpecific.area = 3
    @sessionSpecific.user_id = session[:user_id]

    #Se almacenan los elementos en la tabla temporal

    @sessionTitle.save
    @sessionApproach.save
    @sessionGeneral.save
    @sessionSpecific.save

    #Se obtienen los contadores de errores para cada area

    @titleCounter = params[:thesis][:titleCounter]
    @approachCounter = params[:thesis][:approachCounter]
    @generalPurposeCounter = params[:thesis][:generalCounter]
    @specificPurposeCounter = params[:thesis][:specificCounter]

    @titleOk = 1
    @approachOk = 1
    @generalOk = 1
    @specificOk = 1

    @rules = Rule.all

    #Se verifica que al menos uno de los elementos de la propuesta posee texto

    if ((emptyContent(params[:thesis][:title])) && (emptyContent(params[:thesis][:approach])) && (emptyContent(params[:thesis][:general_purpose])) && (emptyContent(params[:thesis][:specifics])))

      flash[:danger] = "Al menos una de las secciones de la propuesta debe poseer contenido"
      redirect_to(:action => 'thesis_new_repeat_student', :controller => "thesis", :titleCounter => @titleCounter, :approachCounter => @approachCounter, :generalCounter => @generalPurposeCounter, :specificCounter => @specificPurposeCounter) and return

    else

      #Se verifica que el titulo tenga contenido

      if (!emptyContent(@title))

        #Se valida que el titulo cumpla con las reglas de longuitud y palabras claves

        @result = validateThesisArea(@title, "3", "Titulo", -1)

        #Si se incumplio alguna de las reglas

        if (@result.to_s != "NA")

          if @title == "" || @title.nil?

            @title = " "

          end

          if @approach == "" || @approach.nil?

            @approach = " "

          end

          if @generalPurpose == "" || @generalPurpose.nil?

            @generalPurpose = " "

          end

          if params[:thesis][:specifics] == "" || params[:thesis][:specifics].nil?

            @specificsPurposes = " "

          end


          @titleCounter = @titleCounter.to_i + 1
          @titleOk = 0

          flash[:danger] = @result
          redirect_to(:action => 'thesis_new_repeat_student', :controller => "thesis", :titleCounter => @titleCounter, :approachCounter => @approachCounter, :generalCounter => @generalPurposeCounter, :specificCounter => @specificPurposeCounter) and return

        else

          @titleOk = 1

        end

      else

        @titleOk = 1

      end

      #Se valida que el planteamiento tenga contenido

      if (!emptyContent(@approach))


        #Se verifica que el planteamiento cumpla con las reglas

        @result = validateThesisArea(@approach, "3", "Planteamiento", -1)


        #Si el planteamiento incumple alguna de las reglas de longuitud y/o palabras claves

        if (@result.to_s != "NA")

          if @title == "" || @title.nil?

            @title = " "

          end

          if @approach == "" || @approach.nil?

            @approach = " "

          end

          if @generalPurpose == "" || @generalPurpose.nil?

            @generalPurpose = " "

          end

          if params[:thesis][:specifics] == "" || params[:thesis][:specifics].nil?

            @specificsPurposes = " "

          end


          @approachCounter = @approachCounter.to_i + 1
          @approachOk = 0

          flash[:danger] = @result
          redirect_to(:action => 'thesis_new_repeat_student', :controller => "thesis", :titleCounter => @titleCounter, :approachCounter => @approachCounter, :generalCounter => @generalPurposeCounter, :specificCounter => @specificPurposeCounter) and return

        else

          @approachOk = 1

        end

      else

        @approachOk = 1

      end


      #Se verifica que el objetivo general tenga contenido

      if (!emptyContent(@generalPurpose))

        #Se valida que el objetivo general cumpla con las reglas de longuitud y/o palabras claves

        @result = validateThesisArea(@generalPurpose, "3", "Obj_General", -1)

        #Si se incumplieron las reglas del sistema

        if (@result.to_s != "NA")

          if @title == "" || @title.nil?

            @title = " "

          end

          if @approach == "" || @approach.nil?

            @approach = " "

          end

          if @generalPurpose == "" || @generalPurpose.nil?

            @generalPurpose = " "

          end

          if params[:thesis][:specifics] == "" || params[:thesis][:specifics].nil?

            @specificsPurposes = " "

          end


          @generalPurposeCounter = @generalPurposeCounter.to_i + 1
          @generalOk = 0

          flash[:danger] = @result
          redirect_to(:action => 'thesis_new_repeat_student', :controller => "thesis", :titleCounter => @titleCounter, :approachCounter => @approachCounter, :generalCounter => @generalPurposeCounter, :specificCounter => @specificPurposeCounter) and return

        else

          @generalOk = 1

        end

      else

        @generalOk = 1

      end

      #Se verifica que los objetivos especificos tengan contenido

      if (!emptyContent(@specificsPurposes))

        @specificsPurposes.each do |specificPurpose|

          #Se verifica que cada objetivo especifico cumpla con las reglas de longuitud y/o palabras claves

          @result = validateThesisArea(specificPurpose, "3", "Obj_Especifico", -1)

          #Se incumplieron las reglas para objetivos especificos

          if (@result.to_s != "NA")

            if @title == "" || @title.nil?

              @title = " "

            end

            if @approach == "" || @approach.nil?

              @approach = " "

            end

            if @generalPurpose == "" || @generalPurpose.nil?

              @generalPurpose = " "

            end

            if params[:thesis][:specifics] == "" || params[:thesis][:specifics].nil?

              @specificsPurposes = " "

            end


            @specificPurposeCounter = @specificPurposeCounter.to_i + 1
            @specificOk = 0

            flash[:danger] = @result
            redirect_to(:action => 'thesis_new_repeat_student', :controller => "thesis", :titleCounter => @titleCounter, :approachCounter => @approachCounter, :generalCounter => @generalPurposeCounter, :specificCounter => @specificPurposeCounter) and return

          else

            @specificOk = 1

          end

        end

      else

        @specificOk = 1

      end

      #Si no hubo problema con ninguna de las secciones, se procede a generar la extraccion de categorias de la propuesta

      if ((@titleOk == 1) && (@approachOk == 1) && (@generalOk == 1) && (@specificOk == 1))

        #Estos elementos quedaron de manera referencial ya inicialmente se hacía una traducción a ingles para mejor funcionamiento
        # de la librería de la extracción de categorías

        @titleEnglish = ""
        @approachEnglish = ""
        @generalEnglish = ""
        @specificsEnglish = ""

        @textTitle
        @textApproach
        @textGeneral
        @textSpecific

        @thesisStudent = Thesis.new
        @thesisStudent.student_id = session[:user_id]
        @thesisStudent.teacher_id = nil

        if @thesisStudent.save

          @approachSave = Approach.new
          @generalSave = GeneralPurpose.new
          @specificSave = SpecificPurpose.new

          if (!emptyContent(@title))

            #Se almacena en bd el titulo asociado a la tesis

            @titleSave = Title.new
            @titleSave.content = @title
            @titleSave.content_english = @titleEnglish
            @titleSave.thesis_id = @thesisStudent.id
            @titleSave.status = "Pendiente"
            @titleSave.feedback = ""
            @titleSave.feeling = "Pendiente"
            @titleSave.teacher_feeling = "Pendiente"
            @titleSave.save

            #Se extraen las categorias asociadas al titulo
            # para mayor informacion de la librería consultar en
            #https://github.com/domnikl/highscore

            getAndStoreTextKeywords(@title, @thesisStudent.id)

          end

          #Se repite el procedimiento con el planteamiento, se almacena el mismo en BD, luego se procede a extraer las categorias
          # asociadas a su texto, y se procede a buscar en BD, si no existe se crea la palabra clave y se asocia
          # y en caso de existir solo se asocia con la tesis

          if (!emptyContent(@approach))

            @approachSave = Approach.new
            @approachSave.content = @approach
            @approachSave.content_english = @approachEnglish
            @approachSave.thesis_id = @thesisStudent.id
            @approachSave.status = "Pendiente"
            @approachSave.feedback = ""
            @approachSave.feeling = "Pendiente"
            @approachSave.teacher_feeling = "Pendiente"
            @approachSave.save

            getAndStoreTextKeywords(@approach, @thesisStudent.id)

          end


          #Se repite el procedimiento con el objetivo general, se almacena el mismo en BD, luego se procede a extraer las categorias
          # asociadas a su texto, y se procede a buscar en BD, si no existe se crea la palabra clave y se asocia
          # y en caso de existir solo se asocia con la tesis


          if (!emptyContent(@generalPurpose))

            @generalSave = GeneralPurpose.new
            @generalSave.content = @generalPurpose
            @generalSave.content_english = @generalEnglish
            @generalSave.thesis_id = @thesisStudent.id
            @generalSave.status = "Pendiente"
            @generalSave.feedback = ""
            @generalSave.feeling = "Pendiente"
            @generalSave.teacher_feeling = "Pendiente"
            @generalSave.save

            getAndStoreTextKeywords(@generalPurpose, @thesisStudent.id)

          end

          #Se repite el procedimiento con los objetivos especificos, se almacena el mismo en BD, luego se procede a extraer las categorias
          # asociadas a su texto, y se procede a buscar en BD, si no existe se crea la palabra clave y se asocia
          # y en caso de existir solo se asocia con la tesis

          if (!emptyContent(params[:thesis][:specifics]))

            @specificsPurposes.each do |specificPurpose|

              @specificSave = SpecificPurpose.new
              @specificSave.content = specificPurpose
              @specificSave.thesis_id = @thesisStudent.id
              @specificSave.status = "Pendiente"
              @specificSave.feedback = ""
              @specificSave.feeling = "Pendiente"
              @specificSave.version = 1
              @specificSave.teacher_feeling = "Pendiente"
              @specificSave.save

              getAndStoreTextKeywords(specificPurpose, @thesisStudent.id)

            end

          end

          #Posterior a la extraccion de categorias, se procede a mostrar la lista de sugerencias para tutores en el sistema

          redirect_to(:action => 'thesis_teacher_select', :controller => "thesis", :thesis_id => @thesisStudent.id) and return


        else

          flash[:danger] = "Ha ocurrido un problema al intentar guardar su propuesta por favor intente nuevamente"
          redirect_to(:action => 'thesis_new_repeat_student', :controller => "thesis", :titleCounter => @titleCounter, :approachCounter => @approachCounter, :generalCounter => @generalPurposeCounter, :specificCounter => @specificPurposeCounter) and return

        end

      end

    end

  end

  def thesis_create_title

    #Funcion encargada de manejar cuando el estudiante va a agregar su titulo a la propuesta de manera independiente
    # es decir una vez generada la propuesta de trabajo de grado y quiere agregar si titulo que anteriormente no estaba
    # registrado en el sistema

    if (!emptyContent(params[:title][:content]))

      @thesis = Thesis.find(params[:title][:thesisId])

      #Aqui se verifica si la tesis no tiene tutor, en cuyo caso se siguen las reglas dadas por la Coordinacion

      if @thesis.teacher_id.nil?

        #Se verifican las reglas relacionadas con titulo

        @result = validateThesisArea(params[:title][:content], "3", "Titulo", -1)

        #Si se incumplieron las reglas del sistema

        if (@result.to_s != "NA")

          @titleCounter = params[:title][:titleCounter].to_i
          @titleCounter = @titleCounter + 1

          flash[:danger] = @result
          redirect_to(:action => 'register_repeat_title', :controller => "thesis", :title_content => params[:title][:content], :title_counter => @titleCounter, :thesis_id => params[:title][:thesisId]) and return

        end

      else

        #En este caso si hay tutor entonces se verifican las reglas dadas por este y se hacen las validaciones basadas en estas
        # en lugar de guiarse por las de la coordinacion

        if (validateRuleExists("2", "Titulo", @thesis.teacher_id))

          @result = validateThesisArea(params[:title][:content], "2", "Titulo", @thesis.teacher_id)

          #Si se incumplieron las reglas del sistema

          if (@result.to_s != "NA")

            @titleCounter = params[:title][:titleCounter].to_i
            @titleCounter = @titleCounter + 1

            flash[:danger] = @result
            redirect_to(:action => 'register_repeat_title', :controller => "thesis", :title_content => params[:title][:content], :title_counter => @titleCounter, :thesis_id => params[:title][:thesisId]) and return

          end

        else

          #Si la tesis tiene tutor pero el mismo no ha definido reglas se procede a verificar lo que determina la Coordinacion para
          # el area de titulos

          @result = validateThesisArea(params[:title][:content], "3", "Titulo", -1)

          #Si se incumplieron las reglas del sistema

          if (@result.to_s != "NA")

            @titleCounter = params[:title][:titleCounter].to_i
            @titleCounter = @titleCounter + 1

            flash[:danger] = @result
            redirect_to(:action => 'register_repeat_title', :controller => "thesis", :title_content => params[:title][:content], :title_counter => @titleCounter, :thesis_id => params[:title][:thesisId]) and return

          end


        end

      end

      #Si no hubo problema con la revision del titulo se procede entonces a almacenarlo en BD
      # y extraer las categorias asociadas al mismo para alimentar la BD del sistema
      # Se verifica igual si existen las categorias en la BD, si es asi solo se relacionan
      # en caso contrario se almacenan las categorias y se guardan en bd

      @titleSave = Title.new
      @titleSave.content = params[:title][:content]
      @titleSave.content_english = ""
      @titleSave.thesis_id = @thesis.id
      @titleSave.status = "Pendiente"
      @titleSave.feedback = ""
      @titleSave.feeling = "Pendiente"
      @titleSave.teacher_feeling = "Pendiente"

      getAndStoreTextKeywords(params[:title][:content], @thesis.id)

      if @titleSave.save

        flash[:success] = "El título se generó con éxito"
        redirect_to(:action => 'edit_title', :controller => "thesis", :thesis_id => @thesis.id) and return


      else

        flash[:danger] = "Ha ocurrido un error, por favor vuelva a registrar su título"
        redirect_to(:action => 'register_repeat_title', :controller => "thesis", :title_content => params[:title][:content], :title_counter => @titleCounter, :thesis_id => params[:title][:thesisId]) and return

      end


    else

      @titleCounter = params[:title][:titleCounter].to_i
      @titleCounter = @titleCounter + 1
      params[:title][:content] = " "

      flash[:danger] = "El título no puede estar vacío"
      redirect_to(:action => 'register_repeat_title', :controller => "thesis", :title_content => params[:title][:content], :title_counter => @titleCounter, :thesis_id => params[:title][:thesisId]) and return

    end


  end

  def thesis_create_approach

    #Funcion encargada de manejar cuando el estudiante va a agregar su planteamiento a la propuesta de manera independiente
    # es decir una vez generada la propuesta de trabajo de grado y quiere agregar su planteamiento que anteriormente no estaba
    # registrado en el sistema

    if (!emptyContent(params[:approach][:content]))

      #Se verifica si la tesis tiene ya tutor

      @thesis = Thesis.find(params[:approach][:thesisId])

      #Si no posee en este caso se pasa a validar el planteamiento con las reglas de la coordinacion

      if @thesis.teacher_id.nil?

        #Se verifican las reglas relacionadas con plantemiento

        @result = validateThesisArea(params[:approach][:content], "3", "Planteamiento", -1)

        #Si se incumplieron las reglas del sistema

        if (@result.to_s != "NA")

          @approachCounter = params[:approach][:approachCounter].to_i
          @approachCounter = @approachCounter + 1

          flash[:danger] = @result
          redirect_to(:action => 'register_repeat_approach', :controller => "thesis", :approach_content => params[:approach][:content], :approach_counter => @approachCounter, :thesis_id => params[:approach][:thesisId]) and return

        end

      else

        #En este caso si hay tutor entonces se verifican las reglas dadas por este y se hacen las validaciones basadas en estas
        # en lugar de guiarse por las de la coordinacion

        if (validateRuleExists("2", "Planteamiento", @thesis.teacher_id))

          @result = validateThesisArea(params[:approach][:content], "2", "Planteamiento", @thesis.teacher_id)

          #Si se incumplieron las reglas del sistema

          if (@result.to_s != "NA")

            @approachCounter = params[:approach][:approachCounter].to_i
            @approachCounter = @approachCounter + 1

            flash[:danger] = @result
            redirect_to(:action => 'register_repeat_approach', :controller => "thesis", :approach_content => params[:approach][:content], :approach_counter => @approachCounter, :thesis_id => params[:approach][:thesisId]) and return

          end

        else


          #Si la tesis tiene tutor pero el mismo no ha definido reglas se procede a verificar lo que determina la Coordinacion para
          # el area de planteamientos

          @result = validateThesisArea(params[:approach][:content], "3", "Planteamiento", -1)

          #Si se incumplieron las reglas del sistema

          if (@result.to_s != "NA")

            @approachCounter = params[:approach][:approachCounter].to_i
            @approachCounter = @approachCounter + 1

            flash[:danger] = @result
            redirect_to(:action => 'register_repeat_approach', :controller => "thesis", :approach_content => params[:approach][:content], :approach_counter => @approachCounter, :thesis_id => params[:approach][:thesisId]) and return

          end


        end

      end

      #Si el planteamiento esta alineado a las reglas, se procede entonces a extraer las categorias y almacenarlas en BD
      # en caso de no existir, y en ambos casos se asocia a la tesis con dichas areas

      @approachSave = Approach.new
      @approachSave.content = params[:approach][:content]
      @approachSave.content_english = ""
      @approachSave.thesis_id = @thesis.id
      @approachSave.status = "Pendiente"
      @approachSave.feedback = ""
      @approachSave.feeling = "Pendiente"
      @approachSave.teacher_feeling = "Pendiente"

      getAndStoreTextKeywords(params[:approach][:content], @thesis.id)

      #Si se almaceno correctamente el planteamiento se redirige a la vista con las secciones de la tesis

      if @approachSave.save

        flash[:success] = "El planteamiento del problema se generó con éxito"
        redirect_to(:action => 'edit_approach', :controller => "thesis", :thesis_id => @thesis.id) and return


      else

        flash[:danger] = "Ha ocurrido un error, por favor vuelva a registrar su planteamiento del problema"
        redirect_to(:action => 'register_repeat_approach', :controller => "thesis", :approach_content => params[:approach][:content], :approach_counter => @approachCounter, :thesis_id => params[:approach][:thesisId]) and return

      end


    else

      @approachCounter = params[:approach][:approachCounter].to_i
      @approachCounter = @approachCounter + 1
      params[:approach][:content] = " "

      flash[:danger] = "El planteamiento del problema no puede estar vacío"
      redirect_to(:action => 'register_repeat_approach', :controller => "thesis", :approach_content => params[:approach][:content], :approach_counter => @approachCounter, :thesis_id => params[:approach][:thesisId]) and return

    end


  end

  def thesis_create_general_purpose

    #Funcion encargada de manejar cuando el estudiante va a agregar su objetivo general a la propuesta de manera independiente
    # es decir una vez generada la propuesta de trabajo de grado y quiere agregar si objetivo general que anteriormente no estaba
    # registrado en el sistema

    if (!emptyContent(params[:general_purpose][:content]))

      @thesis = Thesis.find(params[:general_purpose][:thesisId])

      #Aqui se verifica si la tesis no tiene tutor, en cuyo caso se siguen las reglas dadas por la Coordinacion

      if @thesis.teacher_id.nil?

        #Se verifican las reglas relacionadas con titulo

        @result = validateThesisArea(params[:general_purpose][:content], "3", "Obj_General", -1)

        #Si se incumplieron las reglas del sistema

        if (@result.to_s != "NA")

          @generalPurposeCounter = params[:general_purpose][:general_purposeCounter].to_i
          @generalPurposeCounter = @generalPurposeCounter + 1

          flash[:danger] = @result
          redirect_to(:action => 'register_repeat_general_purpose', :controller => "thesis", :general_purpose_content => params[:general_purpose][:content], :general_purpose_counter => @generalPurposeCounter, :thesis_id => params[:general_purpose][:thesisId]) and return

        end

      else

        #En este caso si hay tutor entonces se verifican las reglas dadas por este y se hacen las validaciones basadas en estas
        # en lugar de guiarse por las de la coordinacion

        if (validateRuleExists("2", "Obj_General", @thesis.teacher_id))

          @result = validateThesisArea(params[:general_purpose][:content], "2", "Obj_General", @thesis.teacher_id)

          #Si se incumplieron las reglas del sistema

          if (@result.to_s != "NA")

            @generalPurposeCounter = params[:general_purpose][:general_purposeCounter].to_i
            @generalPurposeCounter = @generalPurposeCounter + 1

            flash[:danger] = @result
            redirect_to(:action => 'register_repeat_general_purpose', :controller => "thesis", :general_purpose_content => params[:general_purpose][:content], :general_purpose_counter => @generalPurposeCounter, :thesis_id => params[:general_purpose][:thesisId]) and return

          end

        else

          #Si la tesis tiene tutor pero el mismo no ha definido reglas se procede a verificar lo que determina la Coordinacion para
          # el area de objetivos generales

          @result = validateThesisArea(params[:general_purpose][:content], "3", "Obj_General", -1)

          #Si se incumplieron las reglas del sistema

          if (@result.to_s != "NA")

            @generalPurposeCounter = params[:general_purpose][:general_purposeCounter].to_i
            @generalPurposeCounter = @generalPurposeCounter + 1

            flash[:danger] = @result
            redirect_to(:action => 'register_repeat_general_purpose', :controller => "thesis", :general_purpose_content => params[:general_purpose][:content], :general_purpose_counter => @generalPurposeCounter, :thesis_id => params[:general_purpose][:thesisId]) and return

          end


        end

      end

      #Si no hubo problema con la revision del objetivo general se procede entonces a almacenarlo en BD
      # y extraer las categorias asociadas al mismo para alimentar la BD del sistema
      # Se verifica igual si existen las categorias en la BD, si es asi solo se relacionan
      # en caso contrario se almacenan las categorias y se guardan en bd

      @generalPurposeSave = GeneralPurpose.new
      @generalPurposeSave.content = params[:general_purpose][:content]
      @generalPurposeSave.content_english = ""
      @generalPurposeSave.thesis_id = @thesis.id
      @generalPurposeSave.status = "Pendiente"
      @generalPurposeSave.feedback = ""
      @generalPurposeSave.feeling = "Pendiente"
      @generalPurposeSave.teacher_feeling = "Pendiente"

      getAndStoreTextKeywords(params[:general_purpose][:content], @thesis.id)

      if @generalPurposeSave.save

        flash[:success] = "El objetivo general se generó con éxito"
        redirect_to(:action => 'edit_general_purpose', :controller => "thesis", :thesis_id => @thesis.id) and return


      else

        flash[:danger] = "Ha ocurrido un error, por favor vuelva a registrar su objetivo general"
        redirect_to(:action => 'register_repeat_general_purpose', :controller => "thesis", :general_purpose_content => params[:general_purpose][:content], :general_purpose_counter => @generalPurposeCounter, :thesis_id => params[:general_purpose][:thesisId]) and return

      end


    else

      @generalPurposeCounter = params[:general_purpose][:general_purposeCounter].to_i
      @generalPurposeCounter = @generalPurposeCounter + 1
      params[:general_purpose][:content] = " "

      flash[:danger] = "El objetivo general no puede estar vacío"
      redirect_to(:action => 'register_repeat_general_purpose', :controller => "thesis", :general_purpose_content => params[:general_purpose][:content], :general_purpose_counter => @generalPurposeCounter, :thesis_id => params[:general_purpose][:thesisId]) and return

    end


  end

  def register_repeat_title

    #En caso de que exista incumplimiento de las reglas se lleva el contador para ver si hay que sugerir o no actividades

    if params[:title_content] == " "

      params[:title_content] = ""

    end

    @titleCounter = params[:title_counter].to_i
    @title = Title.new
    @title.content = params[:title_content]
    @thesisId = params[:thesis_id]


  end

  def register_title

    @titleCounter = 0
    @title = Title.new
    @thesisId = params[:thesis_id]


  end

  def edit_title

    #En caso de que la propuesta si tenga titulo, se obtienen la lista de titulos registrados en sistema

    @thesis = Thesis.find(params[:thesis_id])
    @titles = Title.where(thesis_id: params[:thesis_id]).order(created_at: :desc)

  end

  def edit_title_admin

    @thesis = Thesis.find(params[:thesis_id])
    @titles = Title.where(thesis_id: params[:thesis_id]).order(created_at: :desc)

  end

  def register_repeat_approach

    if params[:approach_content] == " "

      params[:approach_content] = ""

    end

    @approachCounter = params[:approach_counter].to_i
    @approach = Approach.new
    @approach.content = params[:approach_content]
    @thesisId = params[:thesis_id]


  end

  def register_repeat_general_purpose

    if params[:general_purpose_content] == " "

      params[:general_purpose_content] = ""

    end

    @generalPurposeCounter = params[:general_purpose_counter].to_i
    @generalPurpose = GeneralPurpose.new
    @generalPurpose.content = params[:general_purpose_content]
    @thesisId = params[:thesis_id]


  end

  def register_approach

    @approachCounter = 0
    @approach = Approach.new
    @thesisId = params[:thesis_id]

  end

  def register_general_purpose

    @generalPurposeCounter = 0
    @generalPurpose = GeneralPurpose.new
    @thesisId = params[:thesis_id]

  end

  def edit_approach

    @thesis = Thesis.find(params[:thesis_id])
    @approach = Approach.where(thesis_id: params[:thesis_id]).order(created_at: :desc)

  end

  def edit_approach_admin

    @thesis = Thesis.find(params[:thesis_id])
    @approach = Approach.where(thesis_id: params[:thesis_id]).order(created_at: :desc)

  end

  def edit_general_purpose

    @thesis = Thesis.find(params[:thesis_id])
    @generalPurpose = GeneralPurpose.where(thesis_id: params[:thesis_id]).order(created_at: :desc)

  end


  def edit_general_purpose_admin

    @thesis = Thesis.find(params[:thesis_id])
    @generalPurpose = GeneralPurpose.where(thesis_id: params[:thesis_id]).order(created_at: :desc)

  end

  def register_specific_purposes

    @specificCounter = 0
    @thesisId = params[:thesis_id]
    @thesis = Thesis.new
    @thesis.specifics = ""

  end


  def register_repeat_specific_purposes

    if params[:specific_specifics] == " "

      params[:specific_specifics] = ""

    end

    @specificCounter = params[:specific_counter]
    @thesisId = params[:thesis_id]
    @thesis = Thesis.new
    @thesis.specifics = params[:specific_specifics]

  end

  def thesis_create_specific

    if (params[:specific][:specifics] != "")

      @thesis = Thesis.find(params[:specific][:thesisId])

      if @thesis.teacher_id.nil?

        @specificRule = Rule.where(user_type: "3", area: "Obj_Especifico").first

        if @specificRule.present?

          @specificWordsRule = RuleKeword.where(id_rule: @specificRule.id)

          @specifics = params[:specific][:specifics].split("*")

          @specifics.each do |specific|

            if ((@specificRule.lenght != 0) && (specific.length > @specificRule.lenght))

              @specificCounter = params[:specific][:specificCounter].to_i
              @specificCounter = @specificCounter + 1

              flash[:danger] = "Los Objetivos Específicos de la propuesta no debe superar los " + @specificRule.lenght.to_s + " carácteres"
              redirect_to(:action => 'register_repeat_specific_purposes', :controller => "thesis", :specific_specifics => params[:specific][:specifics], :specific_counter => @specificCounter, :thesis_id => params[:specific][:thesisId]) and return

            elsif @specificWordsRule.count != 0

              @counterWordSpecific = 0
              @wordsSpecific = ""

              @specificWordsRule.each do |word|

                if @wordsSpecific == ""

                  @wordsSpecific = word.word

                else

                  @wordsSpecific = @wordsSpecific + ", " + word.word

                end

                if specific.upcase.include? word.word.upcase

                  @counterWordSpecific = @counterWordSpecific + 1

                end

              end


              if @counterWordSpecific == 0

                @specificCounter = params[:specific][:specificCounter].to_i
                @specificCounter = @specificCounter + 1

                flash[:danger] = "Los Objetivos Específicos de la propuesta debe contener alguna de las siguientes palabras " + @wordsSpecific
                redirect_to(:action => 'register_repeat_specific_purposes', :controller => "thesis", :specific_specifics => params[:specific][:specifics], :specific_counter => @specificCounter, :thesis_id => params[:specific][:thesisId]) and return

              end

            end

          end

          ######

        end

      else

        @specificRule = Rule.where(user_type: "2", area: "Obj_Especifico", created_by: @thesis.teacher_id).first

        if @specificRule.present?

          @specificWordsRule = RuleKeword.where(id_rule: @specificRule.id)

          @specifics = params[:specific][:specifics].split("*")

          @specifics.each do |specific|

            if ((@specificRule.lenght != 0) && (specific.length > @specificRule.lenght))

              @specificCounter = params[:specific][:specificCounter].to_i
              @specificCounter = @specificCounter + 1

              flash[:danger] = "Los Objetivos Específicos de la propuesta no debe superar los " + @specificRule.lenght.to_s + " carácteres"
              redirect_to(:action => 'register_repeat_specific_purposes', :controller => "thesis", :specific_specifics => params[:specific][:specifics], :specific_counter => @specificCounter, :thesis_id => params[:specific][:thesisId]) and return

            elsif @specificWordsRule.count != 0

              @counterWordSpecific = 0
              @wordsSpecific = ""

              @specificWordsRule.each do |word|

                if @wordsSpecific == ""

                  @wordsSpecific = word.word

                else

                  @wordsSpecific = @wordsSpecific + ", " + word.word

                end

                if specific.upcase.include? word.word.upcase

                  @counterWordSpecific = @counterWordSpecific + 1

                end

              end


              if @counterWordSpecific == 0

                @specificCounter = params[:specific][:specificCounter].to_i
                @specificCounter = @specificCounter + 1

                flash[:danger] = "Los Objetivos Específicos de la propuesta debe contener alguna de las siguientes palabras " + @wordsSpecific
                redirect_to(:action => 'register_repeat_specific_purposes', :controller => "thesis", :specific_specifics => params[:specific][:specifics], :specific_counter => @specificCounter, :thesis_id => params[:specific][:thesisId]) and return

              end

            end

          end

          ###

        else

          @specificRule = Rule.where(user_type: "3", area: "Obj_Especifico").first

          if @specificRule.present?

            @specificWordsRule = RuleKeword.where(id_rule: @specificRule.id)

            @specifics = params[:specific][:specifics].split("*")

            @specifics.each do |specific|

              if ((@specificRule.lenght != 0) && (specific.length > @specificRule.lenght))

                @specificCounter = params[:specific][:specificCounter].to_i
                @specificCounter = @specificCounter + 1

                flash[:danger] = "Los Objetivos Específicos de la propuesta no debe superar los " + @specificRule.lenght.to_s + " carácteres"
                redirect_to(:action => 'register_repeat_specific_purposes', :controller => "thesis", :specific_specifics => params[:specific][:specifics], :specific_counter => @specificCounter, :thesis_id => params[:specific][:thesisId]) and return

              elsif @specificWordsRule.count != 0

                @counterWordSpecific = 0
                @wordsSpecific = ""

                @specificWordsRule.each do |word|

                  if @wordsSpecific == ""

                    @wordsSpecific = word.word

                  else

                    @wordsSpecific = @wordsSpecific + ", " + word.word

                  end

                  if specific.upcase.include? word.word.upcase

                    @counterWordSpecific = @counterWordSpecific + 1

                  end

                end


                if @counterWordSpecific == 0

                  @specificCounter = params[:specific][:specificCounter].to_i
                  @specificCounter = @specificCounter + 1

                  flash[:danger] = "Los Objetivos Específicos de la propuesta debe contener alguna de las siguientes palabras " + @wordsSpecific
                  redirect_to(:action => 'register_repeat_specific_purposes', :controller => "thesis", :specific_specifics => params[:specific][:specifics], :specific_counter => @specificCounter, :thesis_id => params[:specific][:thesisId]) and return

                end

              end

            end

            ######

          end


        end


      end

      @specifics = params[:specific][:specifics].split("*")
      @specifics.each do |specific|

        @specificSave = SpecificPurpose.new
        @specificSave.content = specific
        @specificSave.content_english = ""
        @specificSave.thesis_id = @thesis.id
        @specificSave.status = "Pendiente"
        @specificSave.version = 1
        @specificSave.feedback = ""
        @specificSave.feeling = "Pendiente"
        @specificSave.teacher_feeling = "Pendiente"

        @textSpecific = Highscore::Content.new specific
        @textSpecific.configure do
          set :multiplier, 2
          set :upper_case, 3
          set :long_words, 2
          set :long_words_threshold, 15
          set :short_words_threshold, 3 # => default: 2
          set :bonus_multiplier, 2 # => default: 3
          set :ignore_case, true # => default: false
          set :word_pattern, /[\w]+[^\s0-9]/ # => default: /\w+/
        end

        @textSpecific.keywords.top(50).each do |keywordSpecific|

          @subject = Subject.where(name: keywordSpecific.text)

          if (@subject.first.nil?)

            @subjectSpecific = Subject.new
            @subjectSpecific.name = keywordSpecific.text
            @subjectSpecific.save

            @thesisSpecific = ThesisSubject.new
            @thesisSpecific.thesis_id = @thesis.id
            @thesisSpecific.subject_id = @subjectSpecific.id

            @thesisSpecific.save

          else

            @thesisSubF = ThesisSubject.where(thesis_id: @thesis.id, subject_id: @subject.first.id)

            if @thesisSubF.first.nil?

              @thesisSubject = ThesisSubject.new
              @thesisSubject.thesis_id = @thesis.id
              @thesisSubject.subject_id = @subject.first.id

              @thesisSubject.save

            end


          end

        end

        if @specificSave.save


        else

          flash[:danger] = "Ha ocurrido un error, por favor vuelva a registrar sus Objetivos Específicos"
          redirect_to(:action => 'register_repeat_specific_purposes', :controller => "thesis", :specific_specifics => params[:specific][:specifics], :specific_counter => @specificCounter, :thesis_id => params[:specific][:thesisId]) and return

        end

      end

      flash[:success] = "Los Objetivos Específicos se generaron con éxito"
      redirect_to(:action => 'edit_specific_purposes', :controller => "thesis", :thesis_id => @thesis.id) and return


    else

      @specificCounter = params[:specific][:specificCounter].to_i
      @specificCounter = @specificCounter + 1

      params[:specific][:specifics] = " "

      flash[:danger] = "Los Objetivos Específicos no pueden estar vacíos"
      redirect_to(:action => 'register_repeat_specific_purposes', :controller => "thesis", :specific_specifics => params[:specific][:specifics], :specific_counter => @specificCounter, :thesis_id => params[:specific][:thesisId]) and return

    end

  end


  def modify_specific

    if (params[:specific][:specifics] != "")

      @thesis = Thesis.find(params[:specific][:thesisId])

      if @thesis.teacher_id.nil?

        @specificRule = Rule.where(user_type: "3", area: "Obj_Especifico").first

        if @specificRule.present?

          @specificWordsRule = RuleKeword.where(id_rule: @specificRule.id)

          @specifics = params[:specific][:specifics].split("*")

          @specifics.each do |specific|

            if ((@specificRule.lenght != 0) && (specific.length > @specificRule.lenght))

              @specificCounter = params[:specific][:specificCounter].to_i
              @specificCounter = @specificCounter + 1

              flash[:danger] = "Los Objetivos Específicos de la propuesta no debe superar los " + @specificRule.lenght.to_s + " carácteres"
              redirect_to(:action => 'update_specific', :controller => "thesis", :version => params[:specific][:version], :specific_counter => @specificCounter) and return

            elsif @specificWordsRule.count != 0

              @counterWordSpecific = 0
              @wordsSpecific = ""

              @specificWordsRule.each do |word|

                if @wordsSpecific == ""

                  @wordsSpecific = word.word

                else

                  @wordsSpecific = @wordsSpecific + ", " + word.word

                end

                if specific.upcase.include? word.word.upcase

                  @counterWordSpecific = @counterWordSpecific + 1

                end

              end


              if @counterWordSpecific == 0

                @specificCounter = params[:specific][:specificCounter].to_i
                @specificCounter = @specificCounter + 1

                flash[:danger] = "Los Objetivos Específicos de la propuesta debe contener alguna de las siguientes palabras " + @wordsSpecific
                redirect_to(:action => 'update_specific', :controller => "thesis", :version => params[:specific][:version], :specific_counter => @specificCounter) and return

              end

            end

          end

          ######

        end

      else

        @specificRule = Rule.where(user_type: "2", area: "Obj_Especifico", created_by: @thesis.teacher_id).first

        if @specificRule.present?

          @specificWordsRule = RuleKeword.where(id_rule: @specificRule.id)

          @specifics = params[:specific][:specifics].split("*")

          @specifics.each do |specific|

            if ((@specificRule.lenght != 0) && (specific.length > @specificRule.lenght))

              @specificCounter = params[:specific][:specificCounter].to_i
              @specificCounter = @specificCounter + 1

              flash[:danger] = "Los Objetivos Específicos de la propuesta no debe superar los " + @specificRule.lenght.to_s + " carácteres"
              redirect_to(:action => 'update_specific', :controller => "thesis", :version => params[:specific][:version], :specific_counter => @specificCounter) and return

            elsif @specificWordsRule.count != 0

              @counterWordSpecific = 0
              @wordsSpecific = ""

              @specificWordsRule.each do |word|

                if @wordsSpecific == ""

                  @wordsSpecific = word.word

                else

                  @wordsSpecific = @wordsSpecific + ", " + word.word

                end

                if specific.upcase.include? word.word.upcase

                  @counterWordSpecific = @counterWordSpecific + 1

                end

              end


              if @counterWordSpecific == 0

                @specificCounter = params[:specific][:specificCounter].to_i
                @specificCounter = @specificCounter + 1

                flash[:danger] = "Los Objetivos Específicos de la propuesta debe contener alguna de las siguientes palabras " + @wordsSpecific
                redirect_to(:action => 'update_specific', :controller => "thesis", :version => params[:specific][:version], :specific_counter => @specificCounter) and return

              end

            end

          end

          ###

        else

          @specificRule = Rule.where(user_type: "3", area: "Obj_Especifico").first

          if @specificRule.present?

            @specificWordsRule = RuleKeword.where(id_rule: @specificRule.id)

            @specifics = params[:specific][:specifics].split("*")

            @specifics.each do |specific|

              if ((@specificRule.lenght != 0) && (specific.length > @specificRule.lenght))

                @specificCounter = params[:specific][:specificCounter].to_i
                @specificCounter = @specificCounter + 1

                flash[:danger] = "Los Objetivos Específicos de la propuesta no debe superar los " + @specificRule.lenght.to_s + " carácteres"
                redirect_to(:action => 'update_specific', :controller => "thesis", :version => params[:specific][:version], :specific_counter => @specificCounter) and return

              elsif @specificWordsRule.count != 0

                @counterWordSpecific = 0
                @wordsSpecific = ""

                @specificWordsRule.each do |word|

                  if @wordsSpecific == ""

                    @wordsSpecific = word.word

                  else

                    @wordsSpecific = @wordsSpecific + ", " + word.word

                  end

                  if specific.upcase.include? word.word.upcase

                    @counterWordSpecific = @counterWordSpecific + 1

                  end

                end


                if @counterWordSpecific == 0

                  @specificCounter = params[:specific][:specificCounter].to_i
                  @specificCounter = @specificCounter + 1

                  flash[:danger] = "Los Objetivos Específicos de la propuesta debe contener alguna de las siguientes palabras " + @wordsSpecific
                  redirect_to(:action => 'update_specific', :controller => "thesis", :version => params[:specific][:version], :specific_counter => @specificCounter) and return

                end

              end

            end

            ######

          end


        end


      end

      SpecificPurpose.where(thesis_id: @thesis.id).update_all(status: "Derogado")
      @specifics = params[:specific][:specifics].split("*")
      @specifics.each do |specific|

        @newVersion = params[:specific][:version].to_i + 1

        @specificSave = SpecificPurpose.new
        @specificSave.content = specific
        @specificSave.content_english = ""
        @specificSave.thesis_id = @thesis.id
        @specificSave.status = "Pendiente"
        @specificSave.version = @newVersion
        @specificSave.feedback = ""
        @specificSave.feeling = "Pendiente"
        @specificSave.teacher_feeling = "Pendiente"

        @textSpecific = Highscore::Content.new specific
        @textSpecific.configure do
          set :multiplier, 2
          set :upper_case, 3
          set :long_words, 2
          set :long_words_threshold, 15
          set :short_words_threshold, 3 # => default: 2
          set :bonus_multiplier, 2 # => default: 3
          set :ignore_case, true # => default: false
          set :word_pattern, /[\w]+[^\s0-9]/ # => default: /\w+/
        end

        @textSpecific.keywords.top(50).each do |keywordSpecific|

          @subject = Subject.where(name: keywordSpecific.text)

          if (@subject.first.nil?)

            @subjectSpecific = Subject.new
            @subjectSpecific.name = keywordSpecific.text
            @subjectSpecific.save

            @thesisSpecific = ThesisSubject.new
            @thesisSpecific.thesis_id = @thesis.id
            @thesisSpecific.subject_id = @subjectSpecific.id

            @thesisSpecific.save

          else

            @thesisSubF = ThesisSubject.where(thesis_id: @thesis.id, subject_id: @subject.first.id)

            if @thesisSubF.first.nil?

              @thesisSubject = ThesisSubject.new
              @thesisSubject.thesis_id = @thesis.id
              @thesisSubject.subject_id = @subject.first.id

              @thesisSubject.save

            end


          end

        end

        if @specificSave.save


        else

          flash[:danger] = "Ha ocurrido un error, por favor vuelva a registrar sus Objetivos Específicos"
          redirect_to(:action => 'update_specific', :controller => "thesis", :version => params[:specific][:version], :specific_counter => @specificCounter) and return

        end

      end

      flash[:success] = "Los Objetivos Específicos se modificaron con éxito"
      redirect_to(:action => 'edit_specific_purposes', :controller => "thesis", :thesis_id => @thesis.id) and return


    else

      @specificCounter = params[:specific][:specificCounter].to_i
      @specificCounter = @specificCounter + 1

      params[:specific][:specifics] = " "

      flash[:danger] = "Los Objetivos Específicos no pueden estar vacíos"
      redirect_to(:action => 'update_specific', :controller => "thesis", :version => params[:specific][:version], :specific_counter => @specificCounter) and return

    end


  end

  def show_specific

    @thesisNewSpec = Thesis.where(student_id: session[:user_id])

    @specific = SpecificPurpose.where(version: params[:version], thesis_id: @thesisNewSpec.first.id)
    @thesisId = @specific.first.thesis_id
    @version = params[:version]

    @createdAt = @specific.first.created_at.strftime("%F")
    @status = @specific.first.status
    @teacherFeeling = @specific.first.teacher_feeling
    @fedback = @specific.first.feedback

    @thesis = Thesis.new
    @value = ""
    @specific.each do |specific|

      if @value == ""

        @value = specific.content

      else

        @value = @value + "*" + specific.content

      end

    end

    @thesis.specifics = @value


  end


  def show_specific_admin

    @specific = SpecificPurpose.where(version: params[:version])
    @thesisId = @specific.first.thesis_id
    @version = params[:version]

    @createdAt = @specific.first.created_at.strftime("%F")
    @status = @specific.first.status
    @teacherFeeling = @specific.first.teacher_feeling
    @systemFeeling = @specific.first.feeling
    @fedback = @specific.first.feedback

    @thesis = Thesis.new
    @value = ""
    @specific.each do |specific|

      if @value == ""

        @value = specific.content

      else

        @value = @value + "*" + specific.content

      end

    end

    @thesis.specifics = @value


  end


  def update_specific


    @thesisNewSpec = Thesis.where(student_id: session[:user_id])

    @specific = SpecificPurpose.where(version: params[:version], thesis_id: @thesisNewSpec.first.id)
    @specificCounter = params[:specific_counter].to_i
    @thesisId = @specific.first.thesis_id
    @version = params[:version]

    @createdAt = @specific.first.created_at.strftime("%F")
    @status = @specific.first.status
    @teacherFeeling = @specific.first.teacher_feeling
    @fedback = @specific.first.feedback

    @thesis = Thesis.new
    @value = ""
    @specific.each do |specific|

      if @value == ""

        @value = specific.content

      else

        @value = @value + "*" + specific.content

      end

    end

    @thesis.specifics = @value


  end

  def edit_specific_purposes

    @thesis = Thesis.find(params[:thesis_id])
    @specificPurposesCounts = SpecificPurpose.where(thesis_id: params[:thesis_id]).order(created_at: :desc).group("version").count("id")
    @specificPurposes = SpecificPurpose.where(thesis_id: params[:thesis_id]).order(created_at: :desc)


  end

  def edit_specific_purposes_admin

    @thesis = Thesis.find(params[:thesis_id])
    @specificPurposesCounts = SpecificPurpose.where(thesis_id: params[:thesis_id]).order(created_at: :desc).group("version").count("id")
    @specificPurposes = SpecificPurpose.where(thesis_id: params[:thesis_id]).order(created_at: :desc)


  end

  def edit_thesis_teacher_select

    @topicsList = ThesisSubject.where(thesis_id: params[:thesis_id])
    @teachers = Teacher.all

    @thesisTeacher = Thesis.find(params[:thesis_id])
    @teacherThesis = nil
    if @thesisTeacher.teacher_id.nil?

    else

      @teacherThesis = Teacher.find(@thesisTeacher.teacher_id)

    end

    @thesisId = params[:thesis_id]
    @thesisList = Thesis.where(id: @thesisId)
    @studentId = @thesisList.first.student_id

    @teachersTopics = Array.new

    @topicsList.each do |topics|

      @thesisTopic = ThesisSubject.where(subject_id: topics.subject_id)

      @thesisTopic.each do |thesis|

        if thesis.thesis_id != params[:thesis_id]

          @thesisFound = Thesis.find(thesis.thesis_id)

          if (@thesisFound.teacher_id.nil?)

          else

            @found = false

            @teachersTopics.each do |theacherWithThesis|

              if theacherWithThesis == @thesisFound.teacher_id

                @found = true

              end


            end


            if @found == false

              @teachersTopics.push(@thesisFound.teacher_id)

            end


          end

        end


      end

    end

  end

  def update_approach

    @approach = Approach.find(params[:approach_id])
    @approachCounter = params[:approach_counter].to_i
    @thesisId = @approach.thesis_id

  end

  def update_general_purpose

    @general_purpose = GeneralPurpose.find(params[:general_purpose_id])
    @generalPurpose = GeneralPurpose.find(params[:general_purpose_id])
    @generalPurposeCounter = params[:general_purpose_counter].to_i
    @thesisId = @generalPurpose.thesis_id

  end

  def update_title

    @title = Title.find(params[:title_id])
    @titleCounter = params[:title_counter].to_i
    @thesisId = @title.thesis_id

  end

  def modify_approach

    if (!emptyContent(params[:approach][:content]))

      @approach = Approach.find(params[:approach][:approachId])
      @thesis = Thesis.find(@approach.thesis_id)

      if @thesis.teacher_id.nil?

        #Se verifican las reglas relacionadas con planteamiento

        @result = validateThesisArea(params[:approach][:content], "3", "Planteamiento", -1)

        #Si se incumplieron las reglas del sistema

        if (@result.to_s != "NA")

          @approachCounter = params[:approach][:approachCounter].to_i
          @approachCounter = @approachCounter + 1

          flash[:danger] = @result
          redirect_to(:action => 'update_approach', :controller => "thesis", :approach_id => params[:approach][:approachId], :approach_counter => @approachCounter) and return

        end

      else


        #En este caso si hay tutor entonces se verifican las reglas dadas por este y se hacen las validaciones basadas en estas
        # en lugar de guiarse por las de la coordinacion

        if (validateRuleExists("2", "Planteamiento", @thesis.teacher_id))

          @result = validateThesisArea(params[:approach][:content], "2", "Planteamiento", @thesis.teacher_id)

          #Si se incumplieron las reglas del sistema

          if (@result.to_s != "NA")

            @approachCounter = params[:approach][:approachCounter].to_i
            @approachCounter = @approachCounter + 1

            flash[:danger] = @result
            redirect_to(:action => 'update_approach', :controller => "thesis", :approach_id => params[:approach][:approachId], :approach_counter => @approachCounter) and return

          end

        else

          #Si la tesis tiene tutor pero el mismo no ha definido reglas se procede a verificar lo que determina la Coordinacion para
          # el area de titulos

          @result = validateThesisArea(params[:approach][:content], "3", "Planteamiento", -1)

          #Si se incumplieron las reglas del sistema

          if (@result.to_s != "NA")

            @approachCounter = params[:approach][:approachCounter].to_i
            @approachCounter = @approachCounter + 1

            flash[:danger] = @result
            redirect_to(:action => 'update_approach', :controller => "thesis", :approach_id => params[:approach][:approachId], :approach_counter => @approachCounter) and return

          end


        end

      end

      Approach.where(thesis_id: @approach.thesis_id).update_all(status: "Derogado")

      @approachSave = Approach.new
      @approachSave.content = params[:approach][:content]
      @approachSave.content_english = ""
      @approachSave.thesis_id = @approach.thesis_id
      @approachSave.status = "Pendiente"
      @approachSave.feedback = ""
      @approachSave.feeling = "Pendiente"
      @approachSave.teacher_feeling = "Pendiente"

      getAndStoreTextKeywords(params[:approach][:content], @approach.thesis_id)

      if @approachSave.save

        flash[:success] = "El planteamiento del problema se actualizo con éxito"
        redirect_to(:action => 'edit_approach', :controller => "thesis", :thesis_id => @approach.thesis_id) and return


      else

        flash[:danger] = "Ha ocurrido un error, por favor vuelva a actualizar su planteamiento del problema"
        redirect_to(:action => 'update_approach', :controller => "thesis", :approach_id => params[:approach][:approachId], :approach_counter => @approachCounter) and return

      end

    else

      @approachCounter = params[:approach][:approachCounter].to_i
      @approachCounter = @approachCounter + 1

      flash[:danger] = "El Planteamiento del Problema no puede estar vacío"
      redirect_to(:action => 'update_approach', :controller => "thesis", :approach_id => params[:approach][:approachId], :approach_counter => @approachCounter) and return

    end

  end

  def show_title_message

    @title = Title.find(params[:id_title])

  end

  def show_approach_message

    @approach = Approach.find(params[:id_approach])

  end

  def show_general_purpose_message

    @generalPurpose = GeneralPurpose.find(params[:id_general_approach])

  end

  def show_specific_purposes_message

    @specific = SpecificPurpose.where(version: params[:version])
    @thesisId = @specific.first.thesis_id
    @version = params[:version]

    @createdAt = @specific.first.created_at.strftime("%F")
    @status = @specific.first.status
    @teacherFeeling = @specific.first.teacher_feeling
    @fedback = @specific.first.feedback

    @thesis = Thesis.new
    @value = ""
    @specific.each do |specific|

      if @value == ""

        @value = specific.content

      else

        @value = @value + "*" + specific.content

      end

    end

    @thesis.specifics = @value

  end


  def teacher_response_thesis

    @thesis = Thesis.find(params[:id_thesis])
    @response = params[:response].to_i

    if @response == 1

      @thesis.teacher_id = session[:user_id]
      @thesis.save

      Message.where(id_thesis: params[:id_thesis]).destroy_all
      flash[:success] = "Ha aceptado ser Tutor de esta propuesta"
      redirect_to(:action => 'students_teacher_list', :controller => "students") and return

    else

      Message.where(id_thesis: params[:id_thesis]).destroy_all
      flash[:danger] = "Ha rechazado ser Tutor de esta propuesta"
      redirect_to(:action => 'list_students_requets', :controller => "thesis") and return

    end

  end


  def show_student_thesis

    @thesis = Thesis.find(params[:id_thesis])

    @titles = Title.where(thesis_id: @thesis.id).order(created_at: :desc).first
    @approach = Approach.where(thesis_id: @thesis.id).order(created_at: :desc).first
    @generalPurpose = GeneralPurpose.where(thesis_id: @thesis.id).order(created_at: :desc).first
    @specificPurpose = SpecificPurpose.where(thesis_id: @thesis.id).order(created_at: :desc).first

  end

  def modify_general_purpose

    if (params[:general_purpose][:content] != "")

      @generalPurpose = GeneralPurpose.find(params[:general_purpose][:generalPurposeId])
      @thesis = Thesis.find(@generalPurpose.thesis_id)

      if @thesis.teacher_id.nil?

        @generalPurposeRule = Rule.where(user_type: "3", area: "Obj_General").first

        if @generalPurposeRule.present?

          @generalPurposeWordsRule = RuleKeword.where(id_rule: @generalPurposeRule.id)

          if ((@generalPurposeRule.lenght != 0) && (params[:general_purpose][:content].length > @generalPurposeRule.lenght))

            @generalPurposeCounter = params[:general_purpose][:generalPurposeCounter].to_i
            @generalPurposeCounter = @generalPurposeCounter + 1

            flash[:danger] = "El Objetivo General de la propuesta no debe superar los " + @generalPurposeRule.lenght.to_s + " carácteres"
            redirect_to(:action => 'update_general_purpose', :controller => "thesis", :general_purpose_id => params[:general_purpose][:generalPurposeId], :general_purpose_counter => @generalPurposeCounter) and return

          elsif @generalPurposeWordsRule.count != 0

            @counterWordGeneralPurpose = 0
            @wordsGeneralPurpose = ""

            @generalPurposeWordsRule.each do |word|

              if @wordsGeneralPurpose == ""

                @wordsGeneralPurpose = word.word

              else

                @wordsGeneralPurpose = @wordsGeneralPurpose + ", " + word.word

              end

              if params[:general_purpose][:content].upcase.include? word.word.upcase

                @counterWordGeneralPurpose = @counterWordGeneralPurpose + 1

              end

            end


            if @counterWordGeneralPurpose == 0

              @generalPurposeCounter = params[:general_purpose][:generalPurposeCounter].to_i
              @generalPurposeCounter = @generalPurposeCounter + 1

              flash[:danger] = "El Objetivo General de la propuesta debe contener alguna de las siguientes palabras " + @wordsGeneralPurpose
              redirect_to(:action => 'update_general_purpose', :controller => "thesis", :general_purpose_id => params[:general_purpose][:generalPurposeId], :general_purpose_counter => @generalPurposeCounter) and return

            end

          end

        end

      else

        @generalPurposeRule = Rule.where(user_type: "2", area: "Obj_General", created_by: @thesis.teacher_id).first

        if @generalPurposeRule.present?

          @generalPurposeWordsRule = RuleKeword.where(id_rule: @generalPurposeRule.id)

          if ((@generalPurposeRule.lenght != 0) && (params[:general_purpose][:content].length > @generalPurposeRule.lenght))

            @generalPurposeCounter = params[:general_purpose][:generalPurposeCounter].to_i
            @generalPurposeCounter = @generalPurposeCounter + 1

            flash[:danger] = "El Objetivo General de la propuesta no debe superar los " + @generalPurposeRule.lenght.to_s + " carácteres"
            redirect_to(:action => 'update_general_purpose', :controller => "thesis", :general_purpose_id => params[:general_purpose][:generalPurposeId], :general_purpose_counter => @generalPurposeCounter) and return

          elsif @generalPurposeWordsRule.count != 0

            @counterWordGeneralPurpose = 0
            @wordsGeneralPurpose = ""

            @generalPurposeWordsRule.each do |word|

              if @wordsGeneralPurpose == ""

                @wordsGeneralPurpose = word.word

              else

                @wordsGeneralPurpose = @wordsGeneralPurpose + ", " + word.word

              end

              if params[:general_purpose][:content].upcase.include? word.word.upcase

                @counterWordGeneralPurpose = @counterWordGeneralPurpose + 1

              end

            end


            if @counterWordGeneralPurpose == 0

              @generalPurposeCounter = params[:general_purpose][:generalPurposeCounter].to_i
              @generalPurposeCounter = @generalPurposeCounter + 1

              flash[:danger] = "El Objetivo General de la propuesta debe contener alguna de las siguientes palabras " + @wordsGeneralPurpose
              redirect_to(:action => 'update_general_purpose', :controller => "thesis", :general_purpose_id => params[:general_purpose][:generalPurposeId], :general_purpose_counter => @generalPurposeCounter) and return

            end

          end

        else

          @generalPurposeWordsRule = Rule.where(user_type: "3", area: "Obj_General").first

          if @generalPurposeRule.present?

            @generalPurposeWordsRule = RuleKeword.where(id_rule: @approachRule.id)

            if ((@generalPurposeRule.lenght != 0) && (params[:general_purpose][:content].length > @generalPurposeRule.lenght))

              @generalPurposeCounter = params[:general_purpose][:approachCounter].to_i
              @generalPurposeCounter = @generalPurposeCounter + 1

              flash[:danger] = "El Objetivo General de la propuesta no debe superar los " + @generalPurposeRule.lenght.to_s + " carácteres"
              redirect_to(:action => 'update_general_purpose', :controller => "thesis", :general_purpose_id => params[:general_purpose][:generalPurposeId], :general_purpose_counter => @generalPurposeCounter) and return

            elsif @generalPurposeWordsRule.count != 0

              @counterWordGeneralPurpose = 0
              @wordsGeneralPurpose = ""

              @generalPurposeWordsRule.each do |word|

                if @wordsGeneralPurpose == ""

                  @wordsGeneralPurpose = word.word

                else

                  @wordsGeneralPurpose = @wordsGeneralPurpose + ", " + word.word

                end

                if params[:general_purpose][:content].upcase.include? word.word.upcase

                  @counterWordGeneralPurpose = @counterWordGeneralPurpose + 1

                end

              end


              if @counterWordGeneralPurpose == 0

                @generalPurposeCounter = params[:general_purpose][:generalPurposeCounter].to_i
                @generalPurposeCounter = @generalPurposeCounter + 1

                flash[:danger] = "El Objetivo General de la propuesta debe contener alguna de las siguientes palabras " + @wordsGeneralPurpose
                redirect_to(:action => 'update_general_purpose', :controller => "thesis", :general_purpose_id => params[:general_purpose][:generalPurposeId], :general_purpose_counter => @generalPurposeCounter) and return

              end

            end

          end


        end


      end

      GeneralPurpose.where(thesis_id: @generalPurpose.thesis_id).update_all(status: "Derogado")

      @generalPurposeSave = GeneralPurpose.new
      @generalPurposeSave.content = params[:general_purpose][:content]
      @generalPurposeSave.content_english = ""
      @generalPurposeSave.thesis_id = @generalPurpose.thesis_id
      @generalPurposeSave.status = "Pendiente"
      @generalPurposeSave.feedback = ""
      @generalPurposeSave.feeling = "Pendiente"
      @generalPurposeSave.teacher_feeling = "Pendiente"

      @textGeneralPurpose = Highscore::Content.new params[:general_purpose][:content]
      @textGeneralPurpose.configure do
        set :multiplier, 2
        set :upper_case, 3
        set :long_words, 2
        set :long_words_threshold, 15
        set :short_words_threshold, 3 # => default: 2
        set :bonus_multiplier, 2 # => default: 3
        set :ignore_case, true # => default: false
        set :word_pattern, /[\w]+[^\s0-9]/ # => default: /\w+/
      end

      @textGeneralPurpose.keywords.top(50).each do |keywordGeneralPurpose|

        @subject = Subject.where(name: keywordGeneralPurpose.text)

        if (@subject.first.nil?)

          @subjectGeneralPurpose = Subject.new
          @subjectGeneralPurpose.name = keywordGeneralPurpose.text
          @subjectGeneralPurpose.save

          @thesisGeneralPurpose = ThesisSubject.new
          @thesisGeneralPurpose.thesis_id = @thesis.id
          @thesisGeneralPurpose.subject_id = @subjectGeneralPurpose.id

          @thesisGeneralPurpose.save

        else

          @thesisSubF = ThesisSubject.where(thesis_id: @generalPurpose.thesis_id, subject_id: @subject.first.id)

          if @thesisSubF.first.nil?

            @thesisSubject = ThesisSubject.new
            @thesisSubject.thesis_id = @generalPurpose.thesis_id
            @thesisSubject.subject_id = @subject.first.id

            @thesisSubject.save

          end


        end

      end

      if @generalPurposeSave.save

        flash[:success] = "El Objetivo General se actualizo con éxito"
        redirect_to(:action => 'edit_general_purpose', :controller => "thesis", :thesis_id => @generalPurpose.thesis_id) and return


      else

        flash[:danger] = "Ha ocurrido un error, por favor vuelva a actualizar su Objetivo General"
        redirect_to(:action => 'update_general_purpose', :controller => "thesis", :general_purpose_id => params[:general_purpose][:generalPurposeId], :general_purpose_counter => @generalPurposeCounter) and return

      end


    else

      @generalPurposeCounter = params[:general_purpose][:@generalPurposeCounter].to_i
      @generalPurposeCounter = @generalPurposeCounter + 1

      flash[:danger] = "El Objetivo General no puede estar vacío"
      redirect_to(:action => 'update_general_purpose', :controller => "thesis", :general_purpose_id => params[:general_purpose][:generalPurposeId], :general_purpose_counter => @generalPurposeCounter) and return

    end

  end

  #Funcion para actualizar el titulo de una propuesta

  def modify_title

    if (!emptyContent(params[:title][:content]))

      @title = Title.find(params[:title][:titleId])
      @thesis = Thesis.find(@title.thesis_id)

      if @thesis.teacher_id.nil?

        #Se verifican las reglas relacionadas con titulo

        @result = validateThesisArea(params[:title][:content], "3", "Titulo", -1)

        #Si se incumplieron las reglas del sistema

        if (@result.to_s != "NA")

          @titleCounter = params[:title][:titleCounter].to_i
          @titleCounter = @titleCounter + 1

          flash[:danger] = @result
          redirect_to(:action => 'update_title', :controller => "thesis", :title_id => params[:title][:titleId], :title_counter => @titleCounter) and return

        end

      else

        if (validateRuleExists("2", "Titulo", @thesis.teacher_id))

          @result = validateThesisArea(params[:title][:content], "2", "Titulo", @thesis.teacher_id)

          #Si se incumplieron las reglas del sistema

          if (@result.to_s != "NA")

            @titleCounter = params[:title][:titleCounter].to_i
            @titleCounter = @titleCounter + 1

            flash[:danger] = @result
            redirect_to(:action => 'update_title', :controller => "thesis", :title_id => params[:title][:titleId], :title_counter => @titleCounter) and return

          end

        else

          #Si la tesis tiene tutor pero el mismo no ha definido reglas se procede a verificar lo que determina la Coordinacion para
          # el area de titulos

          @result = validateThesisArea(params[:title][:content], "3", "Titulo", -1)

          #Si se incumplieron las reglas del sistema

          if (@result.to_s != "NA")

            @titleCounter = params[:title][:titleCounter].to_i
            @titleCounter = @titleCounter + 1

            flash[:danger] = @result
            redirect_to(:action => 'update_title', :controller => "thesis", :title_id => params[:title][:titleId], :title_counter => @titleCounter) and return

          end

        end

      end

      Title.where(thesis_id: @title.thesis_id).update_all(status: "Derogado")

      @titleSave = Title.new
      @titleSave.content = params[:title][:content]
      @titleSave.content_english = ""
      @titleSave.thesis_id = @title.thesis_id
      @titleSave.status = "Pendiente"
      @titleSave.feedback = ""
      @titleSave.feeling = "Pendiente"
      @titleSave.teacher_feeling = "Pendiente"

      getAndStoreTextKeywords(params[:title][:content], @title.thesis_id)

      if @titleSave.save

        flash[:success] = "El título se actualizo con éxito"
        redirect_to(:action => 'edit_title', :controller => "thesis", :thesis_id => @title.thesis_id) and return

      else

        flash[:danger] = "Ha ocurrido un error, por favor vuelva a actualizar su título"
        redirect_to(:action => 'update_title', :controller => "thesis", :title_id => params[:title][:titleId], :title_counter => @titleCounter) and return

      end


    else

      @titleCounter = params[:title][:titleCounter].to_i
      @titleCounter = @titleCounter + 1

      flash[:danger] = "El título no puede estar vacío"
      redirect_to(:action => 'update_title', :controller => "thesis", :title_id => params[:title][:titleId], :title_counter => @titleCounter) and return

    end

  end

  def show_title

    @title = Title.find(params[:title_id])

  end

  def show_approach

    @approach = Approach.find(params[:approach_id])

  end

  def show_general_purpose

    @generalPurpose = GeneralPurpose.find(params[:general_purpose_id])

  end

  def show_title_admin

    @title = Title.find(params[:title_id])

  end

  def show_approach_admin

    @approach = Approach.find(params[:approach_id])

  end

  def show_general_purpose_admin

    @generalPurpose = GeneralPurpose.find(params[:general_purpose_id])

  end


  def show_title_detail

    @thesis = Thesis.find(params[:id_thesis])
    @titles = Title.where(thesis_id: params[:id_thesis]).order(created_at: :desc)


  end


  def show_approach_detail

    @thesis = Thesis.find(params[:id_thesis])
    @approach = Approach.where(thesis_id: params[:id_thesis]).order(created_at: :desc)


  end


  def show_general_purpose_detail

    @thesis = Thesis.find(params[:id_thesis])
    @generalPurpose = GeneralPurpose.where(thesis_id: params[:id_thesis]).order(created_at: :desc)

  end

  def show_specific_purposes_detail

    @thesis = Thesis.find(params[:id_thesis])
    @specificPurposesCounts = SpecificPurpose.where(thesis_id: params[:id_thesis]).order(created_at: :desc).group("version").count("id")
    @specificPurposes = SpecificPurpose.where(thesis_id: params[:id_thesis]).order(created_at: :desc)


  end


  def show_title_teacher

    @title = Title.find(params[:title_id])

  end

  def evaluate_title

    @title = Title.find(params[:title_id])

  end


  def title_teacher_evaluation

    @gemUse = ""

    if (params[:title][:feedback] != "")

      @evaluacionEnSpanish = params[:title][:feedback]
      @gemUse = "es"

      if (params[:title][:feedback_english] != "")

        @evaluacionEnEnglish = params[:title][:feedback_english]
        @gemUse = "en"

      else

        @evaluacionEnEnglish = @evaluacionEnSpanish

      end

      @title = Title.find(params[:title][:id])
      @title.feedback = @evaluacionEnSpanish
      @title.content_english = @evaluacionEnEnglish

      ts = TextMood.new(language: @gemUse, ternary_output: true)
      @scoreVal = ts.analyze(@evaluacionEnEnglish)
      @val = ""

      if @scoreVal == 1

        @val = "Positivo"

      elsif @scoreVal == 0

        @val = "Neutral"

      else

        @val = "Negativo"

      end

      @title.status = "Revisado"
      @title.feeling = @val
      @title.save

      redirect_to(:action => 'confirm_title_teacher_evaluation', :controller => "thesis", :title_id => params[:title][:id]) and return


    else

      flash[:danger] = "Debe de escribir algún comentario sobre el título"
      redirect_to(:action => 'evaluate_title', :controller => "thesis", :title_id => params[:title][:id]) and return


    end


  end

  def confirm_title_teacher_evaluation

    @title = Title.find(params[:title_id])

  end

  def confirm_title_teacher_submit_evaluation

    @title = Title.find(params[:title][:id])
    @title.teacher_feeling = params[:title][:teacher_feeling]
    @title.save

    flash[:success] = "El título ha sido evaluado exitósamente"
    redirect_to(:action => 'show_title_detail', :controller => "thesis", :id_thesis => @title.thesis_id) and return

  end

  def show_approach_teacher

    @approach = Approach.find(params[:approach_id])


  end

  def evaluate_approach

    @approach = Approach.find(params[:approach_id])

  end

  def approach_teacher_evaluation

    @gemUse = ""

    if (params[:approach][:feedback] != "")

      @evaluacionEnSpanish = params[:approach][:feedback]
      @gemUse = "es"

      if (params[:approach][:feedback_english] != "")

        @evaluacionEnEnglish = params[:approach][:feedback_english]
        @gemUse = "en"

      else

        @evaluacionEnEnglish = @evaluacionEnSpanish

      end

      @approach = Approach.find(params[:approach][:id])
      @approach.feedback = @evaluacionEnSpanish
      @approach.content_english = @evaluacionEnEnglish

      ts = TextMood.new(language: @gemUse, ternary_output: true)
      @scoreVal = ts.analyze(@evaluacionEnEnglish)
      @val = ""

      if @scoreVal == 1

        @val = "Positivo"

      elsif @scoreVal == 0

        @val = "Neutral"

      else

        @val = "Negativo"

      end

      @approach.status = "Revisado"
      @approach.feeling = @val
      @approach.save

      redirect_to(:action => 'confirm_approach_teacher_evaluation', :controller => "thesis", :approach_id => params[:approach][:id]) and return

    else

      flash[:danger] = "Debe de escribir algún comentario sobre el planteamiento del problema"
      redirect_to(:action => 'evaluate_approach', :controller => "thesis", :approach_id => params[:approach][:id]) and return


    end

  end

  def confirm_approach_teacher_evaluation

    @approach = Approach.find(params[:approach_id])


  end

  def confirm_approach_teacher_submit_evaluation

    @approach = Approach.find(params[:approach][:id])
    @approach.teacher_feeling = params[:approach][:teacher_feeling]
    @approach.save

    flash[:success] = "El Planteamiento del problema ha sido evaluado exitósamente"
    redirect_to(:action => 'show_approach_detail', :controller => "thesis", :id_thesis => @approach.thesis_id) and return

  end

  def show_general_purpose_teacher

    @generalPurpose = GeneralPurpose.find(params[:general_purpose_id])


  end

  def evaluate_general_purpose

    @general_purpose = GeneralPurpose.find(params[:general_purpose_id])

  end

  def general_purpose_teacher_evaluation

    @gemUse = ""

    if params[:general_purpose][:feedback] != ""

      @evaluacionEnSpanish = params[:general_purpose][:feedback]
      @gemUse = "es"

      if (params[:general_purpose][:feedback_english] != "")

        @evaluacionEnEnglish = params[:general_purpose][:feedback_english]
        @gemUse = "en"

      else

        @evaluacionEnEnglish = @evaluacionEnSpanish

      end

      @generalPurpose = GeneralPurpose.find(params[:general_purpose][:id])
      @generalPurpose.feedback = @evaluacionEnSpanish
      @generalPurpose.content_english = @evaluacionEnEnglish

      ts = TextMood.new(language: @gemUse, ternary_output: true)
      @scoreVal = ts.analyze(@evaluacionEnEnglish)
      @val = ""

      if @scoreVal == 1

        @val = "Positivo"

      elsif @scoreVal == 0

        @val = "Neutral"

      else

        @val = "Negativo"

      end

      @generalPurpose.feeling = @val
      @generalPurpose.status = "Revisado"
      @generalPurpose.save

      redirect_to(:action => 'confirm_general_purpose_teacher_evaluation', :controller => "thesis", :general_purpose_id => params[:general_purpose][:id]) and return

    else

      flash[:danger] = "Debe de escribir algún comentario sobre el objetivo general"
      redirect_to(:action => 'evaluate_approach', :controller => "thesis", :general_purpose_id => params[:general_purpose][:id]) and return


    end

  end

  def confirm_general_purpose_teacher_evaluation

    @general_purpose = GeneralPurpose.find(params[:general_purpose_id])

  end


  def confirm_general_purpose_teacher_submit_evaluation

    @generalPurpose = GeneralPurpose.find(params[:general_purpose][:id])
    @generalPurpose.teacher_feeling = params[:general_purpose][:teacher_feeling]
    @generalPurpose.save

    flash[:success] = "El Objetivo General del problema ha sido evaluado exitósamente"
    redirect_to(:action => 'show_general_purpose_detail', :controller => "thesis", :id_thesis => @generalPurpose.thesis_id) and return

  end

  def show_specific_purpose_teacher

    @specific = SpecificPurpose.where(version: params[:version])
    @thesisId = @specific.first.thesis_id
    @version = params[:version]

    @createdAt = @specific.first.created_at.strftime("%F")
    @status = @specific.first.status
    @teacherFeeling = @specific.first.teacher_feeling
    @fedback = @specific.first.feedback

    @thesis = Thesis.new
    @value = ""
    @specific.each do |specific|

      if @value == ""

        @value = specific.content

      else

        @value = @value + "*" + specific.content

      end

    end

    @thesis.specifics = @value


  end

  def evaluate_specific_purpose

    @specific = SpecificPurpose.where(version: params[:version])
    @thesisId = @specific.first.thesis_id
    @version = params[:version]

    @createdAt = @specific.first.created_at.strftime("%F")
    @status = @specific.first.status
    @teacherFeeling = @specific.first.teacher_feeling
    @fedback = @specific.first.feedback

    @thesis = Thesis.new
    @value = ""
    @specific.each do |specific|

      if @value == ""

        @value = specific.content

      else

        @value = @value + "*" + specific.content

      end

    end

    @thesis.specifics = @value

  end


  def specific_purpose_teacher_evaluation

    @gemUse = ""

    if (params[:specific_purpose][:feedback] != "")

      @evaluacionEnSpanish = params[:specific_purpose][:feedback]
      @gemUse = "es"

      if (params[:specific_purpose][:feedback_english] != "")

        @evaluacionEnEnglish = params[:specific_purpose][:feedback_english]
        @gemUse = "en"

      else

        @evaluacionEnEnglish = @evaluacionEnSpanish

      end

      @specific = SpecificPurpose.where(version: params[:specific_purpose][:version])

      ts = TextMood.new(language: @gemUse, ternary_output: true)
      @scoreVal = ts.analyze(@evaluacionEnEnglish)
      @val = ""

      if @scoreVal == 1

        @val = "Positivo"

      elsif @scoreVal == 0

        @val = "Neutral"

      else

        @val = "Negativo"

      end

      @specific.each do |specific|

        specific.feedback = @evaluacionEnSpanish
        specific.content_english = @evaluacionEnEnglish

        specific.status = "Revisado"
        specific.feeling = @val
        specific.save

      end

      redirect_to(:action => 'confirm_specific_purpose_teacher_evaluation', :controller => "thesis", :version => params[:specific_purpose][:version]) and return


    else

      flash[:danger] = "Debe de escribir algún comentario sobre los objetivos específicos"
      redirect_to(:action => 'evaluate_specific_purpose', :controller => "thesis", :version => params[:specific_purpose][:version]) and return


    end


  end

  def confirm_specific_purpose_teacher_evaluation

    @specificL = SpecificPurpose.where(version: params[:version])
    @specific = SpecificPurpose.new
    @thesis = Thesis.new
    @value = ""
    @specificL.each do |specific|

      if @value == ""

        @value = specific.content

      else

        @value = @value + "*" + specific.content

      end

    end

    @thesis.specifics = @value

  end


  def confirm_specific_purpose_teacher_submit_evaluation

    @specific = SpecificPurpose.where(version: params[:specific][:version])

    @specific.each do |specific|

      specific.teacher_feeling = params[:specific][:teacher_feeling]
      specific.save

    end


    flash[:success] = "Los Objetivos Específicos ha sido evaluado exitósamente"
    redirect_to(:action => 'show_specific_purposes_detail', :controller => "thesis", :id_thesis => @specific.first.thesis_id) and return

  end


  def index_student

    @thesis = Thesis.where(student_id: session[:user_id]).first

    if (@thesis.nil?)

      @titles = nil
      @approach = nil
      @generalPurpose = nil
      @specificPurpose = nil
      @teacher = nil

    else

      @titles = Title.where(thesis_id: @thesis.id).order(created_at: :desc).first
      @approach = Approach.where(thesis_id: @thesis.id).order(created_at: :desc).first
      @generalPurpose = GeneralPurpose.where(thesis_id: @thesis.id).order(created_at: :desc).first
      @specificPurpose = SpecificPurpose.where(thesis_id: @thesis.id).order(created_at: :desc).first

      if (@thesis.teacher_id.nil?)

        @teacher = nil

      else

        @teacher = Teacher.find(@thesis.teacher_id)

      end

    end

  end

  def show_admin

    @thesis = Thesis.where(student_id: params[:student_id]).first

    if (@thesis.nil?)

      @titles = nil
      @approach = nil
      @generalPurpose = nil
      @specificPurpose = nil
      @teacher = nil

    else

      @titles = Title.where(thesis_id: @thesis.id).order(created_at: :desc).first
      @approach = Approach.where(thesis_id: @thesis.id).order(created_at: :desc).first
      @generalPurpose = GeneralPurpose.where(thesis_id: @thesis.id).order(created_at: :desc).first
      @specificPurpose = SpecificPurpose.where(thesis_id: @thesis.id).order(created_at: :desc).first

      if (@thesis.teacher_id.nil?)

        @teacher = nil

      else

        @teacher = Teacher.find(@thesis.teacher_id)

      end

    end

  end

  def show_section_admin

  end

end
