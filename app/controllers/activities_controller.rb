class ActivitiesController < ApplicationController
  def new

    @activity = Activity.new

  end


  def activities_category_student

    @area = params[:area].to_i
    @areaString = ""
    if @area == 0

      @areaString = "Titulo"

    elsif @area == 1

      @areaString = "Planteamiento"

    elsif @area == 2

      @areaString = "Obj_General"

    else

      @areaString = "Obj_Especifico"

    end

    @activitiesAdmin = Activity.where(status: "Aprobada", area: @areaString, user_type: "3").paginate(:page => params[:page], :per_page => 5)
    @activitiesStudentAdmin = Array.new

    @activitiesAdmin.each do |activity|

      @activityStudent = ActivityStudent.where(id_activity: activity.id, id_student: session[:user_id])

      @activityToPush = ActivityStudent.new
      @activityToPush.id_activity = activity.id
      @activityToPush.id_student = session[:user_id]

      if @activityStudent.count == 0

        @activityToPush.status = "Pendiente"
        @activityToPush.completed = 0

      else

        @activityToPush.status = "Completada"
        @activityToPush.completed = @activityStudent.first.completed

      end

      @activitiesStudentAdmin.push(@activityToPush)


    end

    @activitiesStudentTeacher = nil
    @activitiesTeacher = nil

    @thesis = Thesis.where(student_id: session[:user_id])
    if @thesis.count != 0

      if @thesis.first.teacher_id.nil?

        @activitiesStudentTeacher = nil
        @activitiesTeacher = nil

      else

        @activitiesTeacher = Activity.where(status: "Aprobada", area: @areaString, user_type: "2", proposed_by: @thesis.first.teacher_id).paginate(:page => params[:page], :per_page => 5)
        @activitiesStudentTeacher = Array.new

        @activitiesTeacher.each do |activity|

          @activityStudent = ActivityStudent.where(id_activity: activity.id, id_student: session[:user_id])

          @activityToPush = ActivityStudent.new
          @activityToPush.id_activity = activity.id
          @activityToPush.id_student = session[:user_id]

          if @activityStudent.count == 0

            @activityToPush.status = "Pendiente"
            @activityToPush.completed = 0

          else

            @activityToPush.status = "Completada"
            @activityToPush.completed = @activityStudent.first.completed

          end

          @activitiesStudentTeacher.push(@activityToPush)


        end


      end

    end


  end

  def show_answered_activity

    @origin = params[:category].to_i
    @activity = Activity.find(params[:id])
    @answers = Answer.where(activity_id: params[:id])
    @answersStudent = Array.new
    @userMistake = 0

    @answers.each do |answer|

      @answerStudent = AnswerStudent.where(id_answer: answer.id).first

      if @answerStudent.correct == "No"

        @userMistake = 1

      end

      @answersStudent.push(@answerStudent)

    end

  end

  def answer_activity

    @origin = params[:category].to_i
    @activity = Activity.find(params[:id_activity])
    @answers = Answer.where(activity_id: params[:id_activity])
    @approach = Approach.new

  end


  def submit_answers

    @activity = Activity.find(params[:activity][:id])
    @userAnswers = params[:activity][:userAnswers].split("*")
    @answers = Answer.where(activity_id: params[:activity][:id])
    @origin = params[:activity][:origin]


    AnswerStudent.where(id_student: session[:user_id]).delete_all

    @index = 0
    @userMistakeStudent = 0
    @userAnswers.each do |answerUser|

      @answerUser = AnswerStudent.new
      @answerUser.id_answer = @answers[@index].id
      @answerUser.id_student = session[:user_id]

      if @answers[@index].total.nil?

        @answers[@index].total = 1

      else

        @answers[@index].total = @answers[@index].total.to_i + 1

      end


      @answerCorrect = "No"
      if (@answers[@index].correct == "Si")

        @answerCorrect = "Si"

      end

      if (answerUser == @answerCorrect)

        @answerUser.correct = "Si"

        if @answers[@index].correct_total.nil?

          @answers[@index].correct_total = 1

        else

          @answers[@index].correct_total = @answers[@index].correct_total.to_i + 1

        end

      else

        @answerUser.correct = "No"
        @userMistakeStudent = 1

        if @answers[@index].wrong_total.nil?

          @answers[@index].wrong_total = 1

        else

          @answers[@index].wrong_total = @answers[@index].wrong_total.to_i + 1

        end

      end

      @answers[@index].save

      @answerUser.save
      @index = @index + 1

    end

    ActivityStudent.where(id_activity: @activity.id, id_student: session[:user_id]).delete_all

    @activityUser = ActivityStudent.new
    @activityUser.id_activity = @activity.id
    @activityUser.status = "Completada"
    @activityUser.id_student = session[:user_id]

    if @userMistakeStudent == 1

      @activityUser.completed = 0

    else

      @activityUser.completed = 1

    end

    @activityUser.save

    redirect_to(:action => 'show_answered_activity', :controller => "activities", :id => @activity.id, :category => @origin) and return

  end


  def activities_all_student

    @activities = Activity.where(status: "Aprobada").order(:area).paginate(:page => params[:page], :per_page => 5)
    @activitiesStudent = Array.new

    @activities.each do |activity|

      @activityStudent = ActivityStudent.where(id_activity: activity.id, id_student: session[:user_id])

      @activityToPush = ActivityStudent.new
      @activityToPush.id_activity = activity.id
      @activityToPush.id_student = session[:user_id]


      if @activityStudent.count == 0

        @activityToPush.status = "Pendiente"
        @activityToPush.completed = 0

      else

        @activityToPush.status = "Completada"
        @activityToPush.completed = @activityStudent.first.completed

      end

      @activitiesStudent.push(@activityToPush)


    end

  end

  def create

    @activity = Activity.new(activity_params)

    if (params[:activity][:answers] != "")

      if @activity.save

        @answers = params[:activity][:answers].split("*")

        @answers.each do |answer|

          @answerSplit = answer.split("%")

          @answerRegister = Answer.new
          @answerRegister.text = @answerSplit[0]
          @answerRegister.correct = @answerSplit[1]
          @answerRegister.activity_id = @activity.id
          @answerRegister.save

        end

        flash[:success] = "La actividad ha sido creada éxitosamente"
        @activities = Activity.paginate(:page => params[:page], :per_page => 5)
        @teachers = Teacher.all
        @admins = Admin.all
        render 'index'

      else
        render 'new'
      end

    else

      flash[:danger] = 'Debe introducir al menos una respuesta'
      render 'new'

    end

  end

  def update

    @activity = Activity.find(params[:id])
    if @activity.update_attributes(activity_edit_params)
      flash[:success] = "La actividad ha sido actualizada"

      Answer.where(:activity_id => @activity.id).destroy_all

      @answers = params[:activity][:answers].split("*")
      @answers.each do |answer|

        @answerSplit = answer.split("%")

        @answerRegister = Answer.new
        @answerRegister.text = @answerSplit[0]
        @answerRegister.correct = @answerSplit[1]
        @answerRegister.correct_total = @answerSplit[2].to_i
        @answerRegister.wrong_total = @answerSplit[3].to_i

        @answerRegister.activity_id = @activity.id
        @answerRegister.save

      end

      @activities = Activity.paginate(:page => params[:page], :per_page => 5)
      @teachers = Teacher.all
      @admins = Admin.all

      render 'index'
    else

      flash[:danger] = "No ha sido posible actualizar la actividad"

      @activity = Activity.find(params[:id])
      @answers = Answer.where(activity_id: params[:id]).paginate(:page => params[:page], :per_page => 5)

      render 'edit'
    end

  end

  def edit

    @activity = Activity.find(params[:id])
    @answers = Answer.where(activity_id: params[:id]).paginate(:page => params[:page], :per_page => 5)

  end

  def destroy
  end

  def update_teacher_activity

    @activity = Activity.find(params[:activity][:id])
    if @activity.update_attributes(activity_edit_params)

      Answer.where(:activity_id => @activity.id).destroy_all

      @answers = params[:activity][:answers].split("*")
      @answers.each do |answer|

        @answerSplit = answer.split("%")

        @answerRegister = Answer.new
        @answerRegister.text = @answerSplit[0]
        @answerRegister.correct = @answerSplit[1]

        @answerRegister.correct_total = @answerSplit[2].to_i
        @answerRegister.wrong_total = @answerSplit[3].to_i

        @answerRegister.activity_id = @activity.id
        @answerRegister.save

      end

      flash[:success] = "La actividad ha sido actualizada"
      redirect_to(:action => 'index_teacher', :controller => "activities") and return

    else

      flash[:danger] = "No ha sido posible actualizar la actividad"

      @activity = Activity.find(params[:activity][:id])
      @answers = Answer.where(activity_id: params[:activity][:id])
      @answersString = params[:activity][:answers]

      render 'edit_teacher_activity'

    end

  end

  def edit_teacher_activity

    @activity = Activity.find(params[:id_activity])
    @answers = Answer.where(activity_id: params[:id_activity])

    @answersString = ""
    @answers.each do |answer|

      if (@answersString == "")


        @correctTotal = ""
        @wrongTotal = ""


        if answer.correct_total.nil?

          @correctTotal = "0"

        else

          @correctTotal = answer.correct_total.to_s


        end


        if answer.wrong_total.nil?

          @wrongTotal = "0"

        else

          @wrongTotal = answer.wrong_total.to_s


        end


        @answersString = answer.text + "%" + answer.correct + "%" + @correctTotal + "%" + @wrongTotal

      else


        @correctTotal = ""
        @wrongTotal = ""


        if answer.correct_total.nil?

          @correctTotal = "0"

        else

          @correctTotal = answer.correct_total.to_s


        end


        if answer.wrong_total.nil?

          @wrongTotal = "0"

        else

          @wrongTotal = answer.wrong_total.to_s


        end

        @answersString = @answersString + "*" + answer.text + "%" + answer.correct + "%" + @correctTotal + "%" + @wrongTotal

      end

    end

  end


  def show_teacher_activity

    @activity = Activity.find(params[:id_activity])
    @answers = Answer.where(activity_id: params[:id_activity]).paginate(:page => params[:page], :per_page => 5)

  end


  def destroy_activity_teacher

    Activity.find(params[:id_activity]).destroy
    @answers = Answer.where(activity_id: params[:id_activity])
    @answers.each do |answer|

      AnswerStudent.where(id_answer: answer.id).destroy_all

    end

    ActivityStudent.where(id_activity: params[:id_activity]).destroy_all
    Answer.where(activity_id: params[:id_activity]).destroy_all

    flash[:success] = "Su Actividad ha sido eliminada"
    redirect_to(:action => 'index_teacher', :controller => "activities") and return


  end

  def new_teacher

    @activity = Activity.new

  end

  def create_activity_teacher

    @activity = Activity.new(activity_params)

    if (params[:activity][:answers] != "")

      if @activity.save

        @answers = params[:activity][:answers].split("*")

        @answers.each do |answer|

          @answerSplit = answer.split("%")

          @answerRegister = Answer.new
          @answerRegister.text = @answerSplit[0]
          @answerRegister.correct = @answerSplit[1]
          @answerRegister.activity_id = @activity.id
          @answerRegister.save

        end

        flash[:success] = "La actividad ha sido propuesta éxitosamente"
        redirect_to(:action => 'index_teacher', :controller => "activities") and return


      else
        render 'new_teacher'
      end

    else

      flash[:danger] = 'Debe introducir al menos una respuesta'
      render 'new_teacher'

    end

  end


  def index_teacher

    @activities = Activity.where(proposed_by: session[:user_id], user_type: "2").order(:area).paginate(:page => params[:page], :per_page => 5)

  end

  def index

    @activities = Activity.order(:area).paginate(:page => params[:page], :per_page => 5)
    @teachers = Teacher.all
    @admins = Admin.all

  end

  def show
  end

  def activity_edit_params
    params.require(:activity).permit(:area, :title, :description, :status)
  end

  def activity_params
    params.require(:activity).permit(:area, :title, :description, :proposed_by, :status, :user_type)
  end

end
