class InternalController < ApplicationController
  require 'rubygems'
  require 'translator'
  require 'highscore'
  require 'bing_translator'

  def new

  end


  def update_admin_attributes

    #Actualizacion de los atributos de coordinacion en la plataforma

    if Admin.find(params[:admin][:id]).update_attributes(:email => params[:admin][:email])
      flash[:success] = "El correo ha sido actualizado"
      session[:username] = params[:admin][:email]
      @admin = Admin.find(params[:admin][:id])
      render 'internal_admin_information'

    else

      flash[:danger] = 'No ha sido posible actualizar el correo del administrador'
      @admin = Admin.find(params[:admin][:id])
      render 'internal_admin_information'

    end


  end


  def update_admin_attributes_password

    #Actualizacion de contraseña para coordinacion en la plataforma

    if (params[:admin][:password] != params[:admin][:password_confirm])

      flash[:danger] = "Las contraseñas no coinciden, por favor verificar"
      @admin = Admin.find(params[:admin][:id])
      render 'internal_admin_information'

    else

      if Admin.find(params[:admin][:id]).update_attributes(:password => params[:admin][:password])
        flash[:success] = "La contraseña del administrador ha sido actualizada"
        @admin = Admin.find(params[:admin][:id])
        render 'internal_admin_information'

      else

        flash[:danger] = 'No ha sido posible actualizar la contraseña del administrador'
        @admin = Admin.find(params[:admin][:id])
        render 'internal_admin_information'

      end

    end

  end


  def internal_teacher_information

    #Obtener informacion de profesor en la plataforma

    @teacher = Teacher.find(params[:id])

  end

  def update_teacher

    #Actualizacion de datos del profesor en la plataforma

    if Teacher.find(params[:teacher][:id]).update_attributes(:first_name => params[:teacher][:first_name], :last_name => params[:teacher][:last_name], :email => params[:teacher][:email], :identification_number => params[:teacher][:identification_number], :nationality => params[:teacher][:nationality])
      flash[:success] = "Su información se actualizó con éxito"
      session[:username] = params[:teacher][:first_name] + " " + params[:teacher][:last_name]
      @teacher = Teacher.find(params[:teacher][:id])
      render 'internal_teacher_information'

    else

      flash[:danger] = 'No ha sido posible actualizar su información'
      @teacher = Teacher.find(params[:teacher][:id])
      render 'internal_teacher_information'

    end


  end

  def update_teacher_password

    #Actualizacion de contraseña del profesor en la plataforma

    if (params[:teacher][:password] != params[:teacher][:password_confirm])

      flash[:danger] = "Las contraseñas no coinciden, por favor verificar"
      @teacher = Teacher.find(params[:teacher][:id])
      render 'internal_teacher_information'

    else

      if Teacher.find(params[:teacher][:id]).update_attributes(:password => params[:teacher][:password])
        flash[:success] = "Su contraseña ha sido actualizada"
        @teacher = Teacher.find(params[:teacher][:id])
        render 'internal_teacher_information'

      else

        flash[:danger] = 'No ha sido posible actualizar su contraseña'
        @teacher = Teacher.find(params[:teacher][:id])
        render 'internal_teacher_information'

      end

    end


  end


  def internal_student_information

    #Obtener la informacion del usuario en la plataforma

    @student = Student.find(params[:id])

  end

  def update_student

    #Actualizacion de los datos de estudiante en la plataforma

    if Student.find(params[:student][:id]).update_attributes(:first_name => params[:student][:first_name], :last_name => params[:student][:last_name], :email => params[:student][:email], :student_identification => params[:student][:student_identification])
      flash[:success] = "Su información se actualizó con éxito"
      session[:username] = params[:student][:first_name] + " " + params[:student][:last_name]
      @student = Student.find(params[:student][:id])
      render 'internal_student_information'

    else

      flash[:danger] = 'No ha sido posible actualizar su información'
      @student = Student.find(params[:student][:id])
      render 'internal_student_information'

    end


  end

  def update_student_password

    #Actualizacion de contraseña de usuario tipo estudiante en la plataforma

    if (params[:student][:password] != params[:student][:password_confirm])

      flash[:danger] = "Las contraseñas no coinciden, por favor verificar"
      @student = Student.find(params[:student][:id])
      render 'internal_student_information'

    else

      if Student.find(params[:student][:id]).update_attributes(:password => params[:student][:password])
        flash[:success] = "Su contraseña ha sido actualizada"
        @student = Student.find(params[:student][:id])
        render 'internal_student_information'

      else

        flash[:danger] = 'No ha sido posible actualizar su contraseña'
        @student = Student.find(params[:student][:id])
        render 'internal_student_information'

      end

    end


  end


  def internal_admin_information

    @admin = Admin.find(params[:id])

  end

  def create

    #Funcion de prueba para la integracion con los traductores de Google y Microsoft
    # El BingTranslator -> Es el relacionado con el traductor de Microsoft
    # Las EasyTranslate y GoTranslate son con el API de Google
    # Para ambos casos se hace necesario un Api key con la plataforma correspondiente
    # En ambos casos se muestra aqui como utilizar la libreria de extraccion de categorias
    # y posteriormente el analisis de sentimientos

    ts = TextMood.new(language: "en", ternary_output: true)
    translator = BingTranslator.new('API_DE_MICROSOFT')

    @evaluacionEnSpanish = params[:natural][:evaluacion]

    #@evaluacionEnEnglish = EasyTranslate.translate(@evaluacionEnSpanish, :from => :spanish, :to => :en, :key => 'API_DE_GOOGLE')

    #Esta libreria no se recomienda su uso ya que es previa a la implementacion de Key por parte de Google
    #@evaluacionEnEnglish = GoTranslate.translate(@evaluacionEnSpanish, from: 'es', to: 'en')
    #
    #tr.translate(@evaluacionEnSpanish, "en", "es")

    @evaluacionEnEnglish = translator.translate(@evaluacionEnSpanish, :from => 'es', :to => 'en')

    @text = Highscore::Content.new @evaluacionEnEnglish
    @text.configure do
      set :multiplier, 2
      set :upper_case, 3
      set :long_words, 2
      set :long_words_threshold, 15
      set :short_words_threshold, 3 # => default: 2
      set :bonus_multiplier, 2 # => default: 3
      set :ignore_case, true # => default: false
      set :word_pattern, /[\w]+[^\s0-9]/ # => default: /\w+/
    end


    #Extraccion de categorias en el texto y la importancia que tienen en el contenido

    @text.keywords.top(50).each do |keyword|
      keyword.text # => keyword text
      keyword.weight # => rank weight (float)
    end

    #Evaluacion de sentimiento

    @scoreVal = ts.analyze(@evaluacionEnEnglish)

  end

end
