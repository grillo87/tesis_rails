class Activity < ApplicationRecord



  enum area: [:Titulo, :Planteamiento, :Obj_General, :Obj_Especifico]
  enum status: [:Aprobada, :Pendiente, :Rechazada]

  validates :title, presence: true
  validates :description, presence: true

end
