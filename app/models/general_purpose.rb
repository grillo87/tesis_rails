class GeneralPurpose < ApplicationRecord

  attr_accessor :feedback_english

  enum status: [:Revisado, :Rechazado, :Pendiente, :Derogado]
  enum feeling: [:Positivo, :Negativo, :Neutral, :Pendiente], _suffix: true
  enum teacher_feeling: [:Positivo, :Negativo, :Neutral, :Pendiente], _suffix: true

end
