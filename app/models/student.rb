class Student < ApplicationRecord

  attr_accessor :remember_token, :activation_token, :reset_token

  before_save { self.email = email.downcase }
  has_secure_password
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false }
  validates :student_identification, presence: true,
            uniqueness: { case_sensitive: false }


  def create_reset_digest
    self.reset_token = Student.new_token
    update_attribute(:reset_digest,  Student.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset_student(self).deliver_now
  end

  def Student.new_token
    SecureRandom.urlsafe_base64
  end

  def Student.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
               BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

end
