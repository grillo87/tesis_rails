require 'test_helper'

class ThesisControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get thesis_new_url
    assert_response :success
  end

  test "should get create" do
    get thesis_create_url
    assert_response :success
  end

  test "should get update" do
    get thesis_update_url
    assert_response :success
  end

  test "should get edit" do
    get thesis_edit_url
    assert_response :success
  end

  test "should get destroy" do
    get thesis_destroy_url
    assert_response :success
  end

  test "should get index" do
    get thesis_index_url
    assert_response :success
  end

  test "should get show" do
    get thesis_show_url
    assert_response :success
  end

end
