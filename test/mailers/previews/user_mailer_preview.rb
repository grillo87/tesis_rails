# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/password_reset
  def password_reset_admin
    admin = Admin.first
    admin.reset_token = Admin.new_token
    UserMailer.password_reset_admin(admin)
  end

  def password_reset_student
    student = Student.first
    student.reset_token = Student.new_token
    UserMailer.password_reset_student(student)
  end

  def password_reset_teacher
    teacher = Teacher.first
    teacher.reset_token = Teacher.new_token
    UserMailer.password_reset_teacher(teacher)
  end

end
