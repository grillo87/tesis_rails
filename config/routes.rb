Rails.application.routes.draw do

  get 'thesis/new'

  get 'thesis/create'

  get 'thesis/update'

  get 'thesis/edit'

  get 'thesis/destroy'

  get 'thesis/index'

  get 'thesis/show'

  get 'rules/new'

  get 'rules/create'

  get 'rules/update'

  get 'rules/edit'

  get 'rules/destroy'

  get 'rules/index'

  get 'rules/show'

  resources :activities
  resources :teachers
  resources :students
  resources :rules
  resources :thesis

  get 'activities_category_student/:area', to: "activities#activities_category_student"
  get 'activities_all_student', to: "activities#activities_all_student"
  get 'show_answered_activity/:id/:category', to: "activities#show_answered_activity"
  post '/submit_answers', to: "activities#submit_answers"
  get 'answer_activity/:id_activity/:category', to: "activities#answer_activity"

  post '/thesis_modify_title', to: "thesis#modify_title"
  get 'update_title/:title_id/:title_counter', to: "thesis#update_title"
  get 'show_title/:title_id', to: "thesis#show_title"
  get 'register_title/:thesis_id', to: "thesis#register_title"
  get 'edit_title/:thesis_id', to: "thesis#edit_title"
  get 'register_approach/:thesis_id', to: "thesis#register_approach"
  get 'edit_approach/:thesis_id', to: "thesis#edit_approach"
  get 'register_general_purpose/:thesis_id', to: "thesis#register_general_purpose"
  get 'edit_general_purpose/:thesis_id', to: "thesis#edit_general_purpose"
  get 'register_specific_purposes/:thesis_id', to: "thesis#register_specific_purposes"
  get 'edit_specific_purposes/:thesis_id', to: "thesis#edit_specific_purposes"

  get 'edit_title_admin/:thesis_id', to: "thesis#edit_title_admin"
  get 'edit_approach_admin/:thesis_id', to: "thesis#edit_approach_admin"
  get 'edit_general_purpose_admin/:thesis_id', to: "thesis#edit_general_purpose_admin"
  get 'edit_specific_purposes_admin/:thesis_id', to: "thesis#edit_specific_purposes_admin"

  get 'show_title_admin/:title_id', to: "thesis#show_title_admin"
  get 'show_specific_admin/:version', to: "thesis#show_specific_admin"
  get 'show_approach_admin/:approach_id', to: "thesis#show_approach_admin"
  get 'show_general_purpose_admin/:general_purpose_id', to: "thesis#show_general_purpose_admin"


  get 'update_specific/:version/:specific_counter', to: "thesis#update_specific"
  post '/thesis_modify_specific', to: "thesis#modify_specific"
  post '/thesis_create_specific', to: "thesis#thesis_create_specific"
  get 'show_specific/:version', to: "thesis#show_specific"
  get 'register_repeat_specific_purposes/:thesis_id/:specific_specifics/:specific_counter', to: "thesis#register_repeat_specific_purposes"

  post '/thesis_create_title', to: "thesis#thesis_create_title"
  get 'register_repeat_title/:thesis_id/:title_content/:title_counter', to: "thesis#register_repeat_title"

  post '/thesis_modify_approach', to: "thesis#modify_approach"
  get 'update_approach/:approach_id/:approach_counter', to: "thesis#update_approach"
  get 'show_approach/:approach_id', to: "thesis#show_approach"
  post '/thesis_create_approach', to: "thesis#thesis_create_approach"
  get 'register_repeat_approach/:thesis_id/:approach_content/:approach_counter', to: "thesis#register_repeat_approach"

  post '/thesis_modify_general_purpose', to: "thesis#modify_general_purpose"
  get 'update_general_purpose/:general_purpose_id/:general_purpose_counter', to: "thesis#update_general_purpose"
  get 'show_general_purpose/:general_purpose_id', to: "thesis#show_general_purpose"
  post '/thesis_create_general_purpose', to: "thesis#thesis_create_general_purpose"
  get 'register_repeat_general_purpose/:thesis_id/:general_purpose_content/:general_purpose_counter', to: "thesis#register_repeat_general_purpose"

  get 'student_request_thesis/:teacher_id/:thesis_id/:student_id', to: "teachers#student_request_thesis"
  get 'edit_student_request_thesis/:teacher_id/:thesis_id/:student_id', to: "teachers#edit_student_request_thesis"
  get 'edit_thesis_teacher_select/:thesis_id', to: "thesis#edit_thesis_teacher_select"
  get 'thesis_teacher_select/:thesis_id', to: "thesis#thesis_teacher_select"
  get 'thesis_new_repeat_student/:titleCounter/:approachCounter/:generalCounter/:specificCounter', to: "thesis#thesis_new_repeat_student"
  get 'thesis_new_student', to: "thesis#thesis_new_student"
  post '/thesis_create_student', to: "thesis#thesis_create_student"
  get 'index_student', to: "thesis#index_student"
  get 'thesis_show_admin/:student_id', to: "thesis#show_admin"
  get 'thesis_show_section_admin/:section_id/:type', to: "thesis#show_section_admin"
  get 'rule_teacher/:id', to: "rules#rule_teacher"
  get 'rule_admin/:area', to: "rules#rule_admin"
  post '/update_rule_admin', to: "rules#update_rule_admin"
  get '/activities_admin/:id', to: "teachers#activities_admin"
  get '/activity_teacher_admin/:id/:origin', to: "teachers#activity_teacher_admin"
  get '/update_teacher_activity_admin/:id/:status/:origin', to: "teachers#update_teacher_activity_admin"

  get 'index_teacher', to: "activities#index_teacher"
  get 'new_teacher', to: "activities#new_teacher"
  post '/create_activity_teacher', to: "activities#create_activity_teacher"
  get 'destroy_activity_teacher/:id_activity', to: "activities#destroy_activity_teacher"
  get 'show_teacher_activity/:id_activity', to: "activities#show_teacher_activity"
  get 'edit_teacher_activity/:id_activity', to: "activities#edit_teacher_activity"
  post '/update_teacher_activity', to: "activities#update_teacher_activity"


  get 'index_teacher_rules', to: "rules#index_teacher_rules"
  get 'edit_rules_teacher/:area', to: "rules#edit_rules_teacher"
  post '/update_rule_teacher', to: "rules#update_rule_teacher"


  get '/internal_admin_information/:id', to: "internal#internal_admin_information"
  get '/internal_student_information/:id', to: "internal#internal_student_information"
  get '/internal_teacher_information/:id', to: "internal#internal_teacher_information"
  post '/update_admin', to: "internal#update_admin_attributes"
  post '/update_admin_password', to: "internal#update_admin_attributes_password"
  get '/update_admin', to: "internal#update_admin_attributes"
  get 'update_admin_password', to: "internal#update_admin_attributes_password"

  post '/update_student', to: "internal#update_student"
  post '/update_student_password', to: "internal#update_student_password"

  post '/update_teacher', to: "internal#update_teacher"
  post '/update_teacher_password', to: "internal#update_teacher_password"

  get 'list_students_requets', to: "teachers#list_students_requets"
  get 'show_student_thesis/:id_thesis', to: "thesis#show_student_thesis"

  get 'teacher_response_thesis/:id_thesis/:response', to: "thesis#teacher_response_thesis"
  get 'show_title_message/:id_title', to: "thesis#show_title_message"
  get 'show_approach_message/:id_approach', to: "thesis#show_approach_message"
  get 'show_general_purpose_message/:id_general_approach', to: "thesis#show_general_purpose_message"
  get 'show_specific_purposes_message/:version', to: "thesis#show_specific_purposes_message"

  get 'students_teacher_list', to: "students#students_teacher_list"
  get 'show_thesis_student/:id_student', to: "students#show_thesis_student"

  get 'show_title_detail/:id_thesis', to: "thesis#show_title_detail"
  get 'show_approach_detail/:id_thesis', to: "thesis#show_approach_detail"
  get 'show_general_purpose_detail/:id_thesis', to: "thesis#show_general_purpose_detail"
  get 'show_specific_purposes_detail/:id_thesis', to: "thesis#show_specific_purposes_detail"

  get 'show_title_teacher/:title_id', to: "thesis#show_title_teacher"
  get 'evaluate_title/:title_id', to: "thesis#evaluate_title"
  post '/title_teacher_evaluation', to: "thesis#title_teacher_evaluation"
  get 'confirm_title_teacher_evaluation/:title_id', to: "thesis#confirm_title_teacher_evaluation"
  post '/confirm_title_teacher_submit_evaluation', to: "thesis#confirm_title_teacher_submit_evaluation"

  get 'show_approach_teacher/:approach_id', to: "thesis#show_approach_teacher"
  get 'evaluate_approach/:approach_id', to: "thesis#evaluate_approach"
  post '/approach_teacher_evaluation', to: "thesis#approach_teacher_evaluation"
  get 'confirm_approach_teacher_evaluation/:approach_id', to: "thesis#confirm_approach_teacher_evaluation"
  post '/confirm_approach_teacher_submit_evaluation', to: "thesis#confirm_approach_teacher_submit_evaluation"

  get 'show_general_purpose_teacher/:general_purpose_id', to: "thesis#show_general_purpose_teacher"
  get 'evaluate_general_purpose/:general_purpose_id', to: "thesis#evaluate_general_purpose"
  post '/general_purpose_teacher_evaluation', to: "thesis#general_purpose_teacher_evaluation"
  get 'confirm_general_purpose_teacher_evaluation/:general_purpose_id', to: "thesis#confirm_general_purpose_teacher_evaluation"
  post '/confirm_general_purpose_teacher_submit_evaluation', to: "thesis#confirm_general_purpose_teacher_submit_evaluation"

  get 'show_specific_purpose_teacher/:version', to: "thesis#show_specific_purpose_teacher"
  get 'evaluate_specific_purpose/:version', to: "thesis#evaluate_specific_purpose"
  post '/specific_purpose_teacher_evaluation', to: "thesis#specific_purpose_teacher_evaluation"
  get 'confirm_specific_purpose_teacher_evaluation/:version', to: "thesis#confirm_specific_purpose_teacher_evaluation"
  post '/confirm_specific_purpose_teacher_submit_evaluation', to: "thesis#confirm_specific_purpose_teacher_submit_evaluation"


  root 'sessions#new'

  #sessions controller routes

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get '/logout', to: 'sessions#destroy'
  post '/logout', to: 'sessions#destroy'
  get '/unathorized', to: 'sessions#unathorized'
  post '/unathorized', to: 'sessions#unathorized'

  #password reset controller routes

  resources :password_resets, only: [:new, :create, :edit, :update]
  post '/password_resets/:id', to: 'password_resets#change_password'
  get '/password_resets/edit', to: 'password_resets#edit'


  #internal controller routes

  get '/internal', to: 'internal#new'
  post '/internal', to: 'internal#create'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
